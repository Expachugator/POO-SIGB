﻿

Instrucciones de uso:

1. Hacer doble clic en el icono del archivo TPV.jar. Es imprescindible que la carpeta “listados” se encuentre en el 
mismo directorio que la aplicación, ya que en ella se encuentran los .txt que esta usa para leer/escribir los datos que maneja. Otra opción
 es compilar y ejecutar el archivo TPV.java que se encuentra en el interior del paquete poo.tpv.modelo de la carpeta que contiene el código fuente.


2. Introducir usuario y contraseña en la ventana de login. Puedes usar:

- Usuario: admin, Contraseña: 0000

- Usuario: XULM, Contraseña: 0231