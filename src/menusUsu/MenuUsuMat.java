package menusUsu;

import java.util.Scanner;

import usuarios.Usuario;

//Clase con el menuUsuMat que nos da acceso a otras funciones
public class MenuUsuMat {

	public static void menuUsuMat(Usuario u) {
		Scanner scanner = new Scanner(System.in);

		System.out.println("Elija de que material quiere el listado:");
		System.out.println("1 - Libros");
		System.out.println("2 - Periodicos");
		System.out.println("3 - Revistas");
		System.out.println("4 - Todo");
		System.out.println("0 - Atras");

        //try para evitar que se introduzcan datos de tipo distinto a int
		try {
			int opcion = scanner.nextInt();

			switch (opcion) {
			case 1:
				MenuUsuMatLibro.menuUsuMatLibro(u);
				break;
			case 2:
				MenuUsuMatPeriodico.menuUsuMatPeriodico(u);
				break;
			case 3:
				MenuUsuMatRevista.menuUsuMatRevista(u);
				break;
			case 4:
				MenuUsuMatTodo.menuUsuMatTodo(u);
				break;
			case 0:
				MenuUsu.menuUsu(u);
				break;
			default:
				System.out.println("Introduzca una opcion correcta");
				MenuUsuMat.menuUsuMat(u);
				break;
			}
		} catch (Exception e) {
			System.out.println("Introduza una opcion correcta");
			MenuUsuMat.menuUsuMat(u);
		}
	}
}
