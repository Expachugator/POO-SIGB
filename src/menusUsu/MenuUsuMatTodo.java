package menusUsu;

import java.util.Iterator;
import java.util.Scanner;

import main.FactoriaGestores;
import materiales.GestorLibro;
import materiales.GestorPeriodico;
import materiales.GestorRevista;
import materiales.Libro;
import materiales.Periodico;
import materiales.Revista;
import usuarios.Usuario;

public class MenuUsuMatTodo {

	private static GestorLibro gestorLibro = FactoriaGestores.crearGestorLibro();
	private static GestorPeriodico gestorPeriodico = FactoriaGestores.crearGestorPeriodico();
	private static GestorRevista gestorRevista = FactoriaGestores.crearGestorRevista();

	public static void menuUsuMatTodo(Usuario u) {

		Scanner scanner = new Scanner(System.in);

		imprimirLibros();
		imprimirPeriodicos();
		imprimirRevistas();

		System.out.println("Pulse intro para continuar...");
		String continuar = scanner.next();
		MenuUsuMat.menuUsuMat(u);

	}

	private static void imprimirLibros() {
		Iterator<Libro> it = gestorLibro.getListaLibro().iterator();

    //Se inicializa la lista de libros
		System.out.println("Estos son nuestros libros:");

    //Se recorre imprimiendo sus datos para ver un listado
		while (it.hasNext()) {
			Libro i = it.next();
			System.out.println("Titulo: " + i.getTitulo());
			System.out.println("Autor: " + i.getAutor());
			System.out.println("IdMat: " + i.getIdMat());
			System.out.println("¿Esta prestado?" + i.getPrestado());
			System.out.println("");
		}
	}

	private static void imprimirPeriodicos() {
	//Se inicializa la lista de periodicos
		Iterator<Periodico> it = gestorPeriodico.getListaPeriodico().iterator();

		System.out.println("\nEstos son nuestros periodicos:");

    //Se recorre imprimiendo sus datos para ver un listado
		while (it.hasNext()) {
			Periodico i = it.next();
			System.out.println("Titulo: " + i.getTitulo());
			System.out.println("Fecha" + i.getDia() + "/" + i.getMes() + "/" + i.getAno());
			System.out.println("IdMat: " + i.getIdMat());
			System.out.println("¿Esta prestado?" + i.getPrestado());
			System.out.println("");
		}
	}

	private static void imprimirRevistas() {
	//Se inicializa la lista de revistas
		Iterator<Revista> it = gestorRevista.getListaRevista().iterator();

		System.out.println("\nEstas son nuestras revistas:");

    //Se recorre imprimiendo sus datos para ver un listado
		while (it.hasNext()) {
			Revista i = it.next();
			System.out.println("Titulo: " + i.getTitulo());
			System.out.println("Numero" + i.getNumero());
			System.out.println("IdMat: " + i.getIdMat());
			System.out.println("¿Esta prestado?" + i.getPrestado());
			System.out.println("");
		}
	}
}
