package menusUsu;

import java.util.Iterator;

import main.FactoriaGestores;
import materiales.GestorPeriodico;
import materiales.Periodico;
import usuarios.Usuario;

public class MenuUsuMatPeriodico {

	private static GestorPeriodico gestorPeriodico = FactoriaGestores.crearGestorPeriodico();

	public static void menuUsuMatPeriodico(Usuario u) {

    //Se inicializa la lista de periodicos
		Iterator<Periodico> it = gestorPeriodico.getListaPeriodico().iterator();

		System.out.println("Estos son nuestros periodicos:");

    //Se recorre imprimiendo sus datos para ver un listado
		while (it.hasNext()) {
			Periodico i = it.next();
			System.out.println("Titulo: " + i.getTitulo());
			System.out.println("Fecha" + i.getDia() + "/" + i.getMes() + "/" + i.getAno());
			System.out.println("IdMat: " + i.getIdMat());
			System.out.println("¿Esta prestado?" + i.getPrestado());
			System.out.println("");
		}

		MenuUsuMat.menuUsuMat(u);
	}
}
