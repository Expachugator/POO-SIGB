package menusUsu;

import java.util.Iterator;
import main.FactoriaGestores;
import menusBiblio.MenuBiblioPres;
import prestamos.GestorPrestamo;
import prestamos.Prestamo;
import usuarios.Usuario;

public class MenuUsuHis {

	private static GestorPrestamo gestorPrestamo = FactoriaGestores.crearGestorPrestamo();

	public static void menuUsuHis(Usuario u) {

		System.out.println("Este es su historial de prestamos:");

        //Se inicializa la lista de prestamos
		Iterator<Prestamo> it = gestorPrestamo.getListaPrestamo().iterator();

        //Se va recorriendo buscando cuales son de el usuario en cuestion y se imprime
		while (it.hasNext()) {
			Prestamo i = it.next();
			if (i.getIdUsu() == u.getIdUsuario()) {
				System.out.println("Titulo: " + i.getTitulo());
				System.out.println("IdMat: " + i.getIdMat());
				System.out.println("Fecha de prestamo: " + i.getFechaPrestamo());
				System.out.println("Fecha de devolucion: " + i.getFechaDevolucion());
				System.out.println("");
			}
		}
		MenuBiblioPres.menuBiblioPres();
	}
}
