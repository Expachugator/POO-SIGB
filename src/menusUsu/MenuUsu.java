package menusUsu;

import java.util.Scanner;

import main.Sigb;
import usuarios.Usuario;

public class MenuUsu {

    //Menu principal del usuario, desde aqui nos iremos moviendo por los diferentes menus
	public static void menuUsu(Usuario u) {

		Scanner scanner = new Scanner(System.in);

		System.out.println("Seleccione una opcion");
		System.out.println("1 - Ver ficha");
		System.out.println("2 - Ver materiales");
		System.out.println("3 - Historial prestamos");
		System.out.println("0 - Cerrar sesion");

        //try para evitar que se introduzcan datos de tipo distinto a int
		try {
			int opcion = scanner.nextInt();

			switch (opcion) {
			case 1:
				MenuUsuFicha.menuUsuFicha(u);
				break;
			case 2:
				MenuUsuMat.menuUsuMat(u);
				break;
			case 3:
				MenuUsuHis.menuUsuHis(u);
				break;
			case 0:
				Sigb.imprimirInicio();
				break;
			default:
				System.out.println("Introduzca una opcion correcta");
				MenuUsu.menuUsu(u);
				break;
			}
		} catch (Exception e) {
			System.out.println("Introduza una opcion correcta");
			MenuUsu.menuUsu(u);
		}
	}
}