package menusUsu;

import java.util.Iterator;

import main.FactoriaGestores;
import materiales.GestorLibro;
import materiales.Libro;
import usuarios.Usuario;

public class MenuUsuMatLibro {

	private static GestorLibro gestorLibro = FactoriaGestores.crearGestorLibro();

	public static void menuUsuMatLibro(Usuario u) {

    //Se inicializa la lista de libros
		Iterator<Libro> it = gestorLibro.getListaLibro().iterator();

		System.out.println("Estos son nuestros libros:");

    //Se recorre imprimiendo sus datos para ver un listado
		while (it.hasNext()) {
			Libro i = it.next();
			System.out.println("Titulo: " + i.getTitulo());
			System.out.println("Autor: " + i.getAutor());
			System.out.println("IdMat: " + i.getIdMat());
			System.out.println("¿Esta prestado?" + i.getPrestado());
			System.out.println("");
		}

		MenuUsuMat.menuUsuMat(u);
	}
}
