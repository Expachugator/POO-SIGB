package menusUsu;

import java.util.Iterator;
import java.util.Scanner;

import main.FactoriaGestores;
import materiales.GestorRevista;
import materiales.Revista;
import usuarios.Usuario;

public class MenuUsuMatRevista {

	private static GestorRevista gestorRevista = FactoriaGestores.crearGestorRevista();

	public static void menuUsuMatRevista(Usuario u) {

		Scanner scanner = new Scanner(System.in);

    //Se inicializa la lista de revistas
		Iterator<Revista> it = gestorRevista.getListaRevista().iterator();

		System.out.println("Estos son nuestras revistas:");

    //Se recorre imprimiendo sus datos para ver un listado
		while (it.hasNext()) {
			Revista i = it.next();
			System.out.println("Titulo: " + i.getTitulo());
			System.out.println("Numero" + i.getNumero());
			System.out.println("IdMat: " + i.getIdMat());
			System.out.println("¿Esta prestado?" + i.getPrestado());
			System.out.println("");
		}

		System.out.println("Pulse intro para continuar...");
		String continuar = scanner.nextLine();

		MenuUsuMat.menuUsuMat(u);
	}
}
