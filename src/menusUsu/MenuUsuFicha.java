package menusUsu;

import usuarios.Usuario;

public class MenuUsuFicha {

    //Se imprime la ficha del usuario obteniendo los datos con getters
	public static void menuUsuFicha(Usuario u) {

		System.out.println("--------------------------------------------------");
		System.out.println("|                                                |");
		System.out.println("|                                                |");
		System.out.print("|  Nombre:            " + u.getNombre());
		int tama�o = u.getNombre().length();
		do {
			System.out.print(" ");
			tama�o++;
		} while (tama�o < 27);
		System.out.println("|");
		System.out.print("|  Apellidos:         " + u.getApellidos());
		tama�o = u.getApellidos().length();
		do {
			System.out.print(" ");
			tama�o++;
		} while (tama�o < 27);
		System.out.println("|");
		System.out.print("|  DNI:               " + u.getDni());
		tama�o = u.getDni().length();
		do {
			System.out.print(" ");
			tama�o++;
		} while (tama�o < 27);
		System.out.println("|");
		System.out.print("|  Contraseña:        " + u.getContrasena());
		tama�o = u.getContrasena().length();
		do {
			System.out.print(" ");
			tama�o++;
		} while (tama�o< 27);
		System.out.println("|");
		System.out.print("|  Id Usuario:        " + u.getIdUsuario());
		tama�o = String.valueOf(u.getIdUsuario()).length();
		do {
			System.out.print(" ");
			tama�o++;
		} while (tama�o < 27);
		System.out.println("|");
		System.out.println("|  Mat prestados:     " + u.getPrestamos() + "                          |");
		if (u.getSancion() != null) {
			System.out.println("|  Sancionado hasta:  " + u.getSancion() + "                 |");
		}
		System.out.println("|                                                |");
		System.out.println("|                                                |");
		System.out.println("--------------------------------------------------");

		MenuUsu.menuUsu(u);
	}
}
