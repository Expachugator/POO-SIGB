package suscripciones;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.*;

//Clase con los getter y setter que hacen referencia a las suscripciones, asi como las principales acciones

public class GestorSuscripcion {

	private List<Suscripcion> listaSuscripcion;

	public GestorSuscripcion() {
		listaSuscripcion = new ArrayList<Suscripcion>();
	}

	public void setListaSuscripcion(List<Suscripcion> listaSuscripcion) {
		this.listaSuscripcion = listaSuscripcion;
	}

	public List<Suscripcion> getListaSuscripcion() {
		return listaSuscripcion;
	}

	public void guardarSuscripcion() {
		try {
			ObjectOutputStream guardarListaSuscripcion = new ObjectOutputStream(
					new FileOutputStream("ListaSuscripcion.dat"));
			guardarListaSuscripcion.writeObject(listaSuscripcion);
			guardarListaSuscripcion.close();
		} catch (Exception e) {
			System.out.println("No fue posible guardar listaSuscripcion");
		}
	}

	public void anadirSuscripcion(Suscripcion suscripcionDatos) {
		listaSuscripcion.add(suscripcionDatos);
		guardarSuscripcion();
	}
}
