package suscripciones;

import java.io.Serializable;
import java.util.Iterator;

import main.FactoriaGestores;

//Clase con los parametros correspondientes a las suscripciones

public class Suscripcion implements Serializable {

	private static GestorSuscripcion gestorSuscripcion = FactoriaGestores.crearGestorSuscripcion();

	int idSus;
	String tipoMat;
	String titulo;
	String tipo;
	String fechaInicio;
	String fechaFin;

	public int getIdSus() {
		return idSus;
	}

	public String getTipoMat() {
		return tipoMat;
	}

	public String getTitulo() {
		return titulo;
	}

	public String getTipo() {
		return tipo;
	}

	public String getFechaInicio() {
		return fechaInicio;
	}

	public String getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}

	public Suscripcion(String tipoMat, String titulo, String tipo, String fechaInicio, String fechaFin) {
		//Se crea la variable idGlobal que obendra el id asignado a la ultima suscripcion
		int idGlobal = 0;
		Iterator<Suscripcion> it = gestorSuscripcion.getListaSuscripcion().iterator();
		while (it.hasNext()) {
			Suscripcion i = it.next();
			idGlobal = i.getIdSus();
		}
		this.idSus = idGlobal + 1;
		this.tipoMat = tipoMat;
		this.titulo = titulo;
		this.tipo = tipo;
		this.fechaInicio = fechaInicio;
		this.fechaFin = fechaFin;
		System.out.println("Id asignado: " + this.idSus);
	}

	public Suscripcion(String tipoMat, String titulo, String tipo, String fechaInicio, String fechaFin, int idSus) {

		this.idSus = idSus;
		this.tipoMat = tipoMat;
		this.titulo = titulo;
		this.tipo = tipo;
		this.fechaInicio = fechaInicio;
		this.fechaFin = fechaFin;
	}
}