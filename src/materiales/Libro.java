package materiales;

// Clase con los parametros correspondientes a los libros

public final class Libro extends Material {

	private final String editorial;
	private final String isbn;
	private final String genero;
	private final String autor;

	public String getEditorial() {
		return editorial;
	}

	public String getGenero() {
		return genero;
	}

	public String getAutor() {
		return autor;
	}

	public String getIsbn() {
		return isbn;
	}

	public Libro(String titulo, String genero, String autor, boolean prestado, String editorial, String isbn) {
		super(titulo, prestado);// , idMat);
		this.editorial = editorial;
		this.genero = genero;
		this.autor = autor;
		this.isbn = isbn;
	}

	public void setPrestado(Boolean prestado) {
		this.prestado = prestado;
	}
}