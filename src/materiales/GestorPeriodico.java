package materiales;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.*;

//Clase con los getter y setter que hacen referencia a los periodicos, asi como las principales acciones

public class GestorPeriodico {

	private List<Periodico> listaPeriodico;

	public GestorPeriodico() {
		listaPeriodico = new ArrayList<Periodico>();
	}

	public void setListaPeriodico(List<Periodico> listaPeriodico) {
		this.listaPeriodico = listaPeriodico;
	}

	public List<Periodico> getListaPeriodico() {
		return listaPeriodico;
	}

	public void guardarListaPeriodico() {
		try {
			ObjectOutputStream guardarListaPeriodico = new ObjectOutputStream(
					new FileOutputStream("listaPeriodico.dat"));
			guardarListaPeriodico.writeObject(listaPeriodico);
			guardarListaPeriodico.close();
		} catch (Exception e) {
			System.out.println("No fue posible guardar listaPeriodico");
		}
	}

	public void anadirPeriodico(Periodico periodico) {
		listaPeriodico.add(periodico);
		guardarListaPeriodico();
	}

	public void borrarPeriodico(Periodico i) {
		listaPeriodico.remove(i);
		guardarListaPeriodico();
	}
}