package materiales;

//Clase con los parametros correspondientes a las revistas

public final class Revista extends Material {

	private final String numero;
	private final String mes;
	private final String ano;
	private final String genero;

	public String getGenero() {
		return genero;
	}

	public String getNumero() {
		return numero;
	}

	public String getMes() {
		return mes;
	}

	public String getAno() {
		return ano;
	}

	public void setPrestado(Boolean prestado) {
		this.prestado = prestado;
	}

	public Revista(String titulo, String genero, Boolean prestado, String numero, String mes, String ano) { // String
																											// idMat,
																											// String
																											// numero,
																											// String
																											// mes,
																											// String
																											// ano)
																											// {
		super(titulo, prestado);
		this.genero = genero;
		this.numero = numero;
		this.mes = mes;
		this.ano = ano;
	}
}