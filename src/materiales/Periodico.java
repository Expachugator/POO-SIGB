package materiales;

//Clase con los parametros correspondientes a los periodicos

public final class Periodico extends Material {

	private final String dia;
	private final String mes;
	private final String ano;

	public String getDia() {
		return dia;
	}

	public String getMes() {
		return mes;
	}

	public String getAno() {
		return ano;
	}

	public void setPrestado(Boolean prestado) {
		this.prestado = prestado;
	}

	public Periodico(String titulo, Boolean prestado, String dia, String mes, String ano) {
		super(titulo, prestado);
		this.dia = dia;
		this.mes = mes;
		this.ano = ano;
	}
}
