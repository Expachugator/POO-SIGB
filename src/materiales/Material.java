package materiales;

import java.io.Serializable;
import java.util.Iterator;

import main.FactoriaGestores;

// Clase material con los parametros comunes a todos los materiales

public abstract class Material implements Serializable {

	private static GestorLibro gestorLibro = FactoriaGestores.crearGestorLibro();
	private static GestorPeriodico gestorPeriodico = FactoriaGestores.crearGestorPeriodico();
	private static GestorRevista gestorRevista = FactoriaGestores.crearGestorRevista();

	String titulo;
	private final int idMat;
	boolean prestado = false;

	public String getTitulo() {
		return titulo;
	}

	public int getIdMat() {
		return idMat;
	}

	public boolean getPrestado() {
		return prestado;
	}

	public Material(String titulo, Boolean prestado) {
	//Se crea la variable idGlobal que obendra el id asignado a el ultimo material y una de cada tipo de material
		int idGlobal = 0;
		int idGlobalLibro = 0;
		int idGlobalRevista = 0;
		int idGlobalPeriodico = 0;
		Iterator<Libro> itLibro = gestorLibro.getListaLibro().iterator();
		while (itLibro.hasNext()) {
			Libro i = itLibro.next();
			idGlobalLibro = i.getIdMat();
		}
		Iterator<Periodico> itPer = gestorPeriodico.getListaPeriodico().iterator();
		while (itPer.hasNext()) {
			Periodico i = itPer.next();
			idGlobalPeriodico = i.getIdMat();
		}
		Iterator<Revista> itRev = gestorRevista.getListaRevista().iterator();
		while (itRev.hasNext()) {
			Revista i = itRev.next();
			idGlobalRevista = i.getIdMat();
		}
		//Se asigna a idGlobal el id que sea mayor de cada uno de los distintos materiales
		if (idGlobalLibro > idGlobal) {
			idGlobal = idGlobalLibro;
		}
		if (idGlobalPeriodico > idGlobal) {
			idGlobal = idGlobalPeriodico;
		}
		if (idGlobalRevista > idGlobal) {
			idGlobal = idGlobalRevista;
		}
		this.titulo = titulo;
		this.idMat = idGlobal + 1;
		this.prestado = prestado;
	}
}
