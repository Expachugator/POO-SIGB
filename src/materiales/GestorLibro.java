package materiales;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;

// Clase con los getter y setter que hacen referencia a los libros, asi como las principales acciones

public class GestorLibro{

	private List<Libro> listaLibro;

	public GestorLibro() {
		listaLibro = new ArrayList<Libro>();
	}

	public void setListaLibro(List<Libro> listaLibro) {
		this.listaLibro = listaLibro;
	}

	public List<Libro> getListaLibro() {
		return listaLibro;
	}

	public void guardarListaLibro() {
		try {
			ObjectOutputStream guardarListaLibro = new ObjectOutputStream(new FileOutputStream("listaLibro.dat"));
			guardarListaLibro.writeObject(listaLibro);
			guardarListaLibro.close();
		} catch (Exception e) {
			System.out.println("No fue posible guardar listaLibro");
		}
	}

	public void anadirLibro(Libro libro) {
		listaLibro.add(libro);
		guardarListaLibro();
	}

	public void borrarLibro(Libro i) {
		listaLibro.remove(i);
		guardarListaLibro();
	}
	
	public void exportarListaLibro() {
	    String fechaHoy;
		Date fecha = new Date();
		SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
		fechaHoy = formatoFecha.format(fecha);
		
		try {
			ObjectOutputStream guardarListaLibro = new ObjectOutputStream(new FileOutputStream(fechaHoy + "-listaLibro.dat"));
			guardarListaLibro.writeObject(listaLibro);
			guardarListaLibro.close();
		} catch (Exception e) {
			System.out.println("No fue posible exportar listaLibro");
		}
	}
}
