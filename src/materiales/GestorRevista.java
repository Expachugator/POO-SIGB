package materiales;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.*;

//Clase con los getter y setter que hacen referencia a las revistas, asi como las principales acciones

public class GestorRevista{

	private List<Revista> listaRevista;

	public GestorRevista() {
		listaRevista = new ArrayList<Revista>();
	}

	public void setListaRevista(List<Revista> listaRevista) {
		this.listaRevista = listaRevista;
	}

	public List<Revista> getListaRevista() {
		return listaRevista;
	}

	public void guardarListaRevista() {
		try {
			ObjectOutputStream guardarListaRevista = new ObjectOutputStream(new FileOutputStream("listaRevista.dat"));
			guardarListaRevista.writeObject(listaRevista);
			guardarListaRevista.close();
		} catch (Exception e) {
			System.out.println("No fue posible guardar listaRevista");
		}
	}

	public void anadirRevista(Revista revista) {
		listaRevista.add(revista);
		guardarListaRevista();
	}

	public void borrarRevista(Revista i) {
		listaRevista.remove(i);
		guardarListaRevista();
	}
}