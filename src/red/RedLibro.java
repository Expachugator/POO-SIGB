package red;

public class RedLibro extends RedMaterial {

	private final String editorial;
	private final String isbn;
	private final String genero;
	private final String autor;
	
	public String getEditorial() {
		return editorial;
	}

	public String getGenero() {
		return genero;
	}

	public String getAutor() {
		return autor;
	}

	public String getIsbn() {
		return isbn;
	}

	public RedLibro(String titulo, String genero, String autor, String editorial, String isbn, int idMat, String "Las Tablas") {
	    super (idMat, titulo);
		this.editorial = editorial;
		this.genero = genero;
		this.autor = autor;
		this.isbn = isbn;
	}
}
