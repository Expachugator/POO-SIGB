package red;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class GestorRedPeriodico {
	
	private List<Periodico> listaExpPeriodico;
	private List<Periodico> listaImpMostolesPeriodico;
    private List<Periodico> listaImpCosladaPeriodico;

	public GestorRedPeriodico() {
		listaExpPeriodico = new ArrayList<Periodico>();
		listaImpMostolesPeriodico = new ArrayList<Periodico>();
		listaImpCosladaPeriodico = new ArrayList<Periodico>();
	}

	public void setListaExpPeriodico(List<Periodico> listaExpPeriodico) {
		this.listaExpPeriodico = listaExpPeriodico;
	}
	
	public void setListaImpMostolesPeriodico(List<Periodico> listaImpMostolesPeriodico) {
		this.listaImpMostolesPeriodico = listaImpMostolesPeriodico;
	}

    public void setListaImpCosladaPeriodico(List<Periodico> listaImpCosladaPeriodico) {
		this.listaImpCosladaPeriodico = listaImpCosladaPeriodico;
	}

	public List<Periodico> getListaExpPeriodico() {
		return listaExpPeriodico;
	}
	
	public List<Periodico> getListaImpMostolesPeriodico() {
		return listaImpMostolesPeriodico;
	}
	
	public List<Periodico> getListaImpCosladaPeriodico() {
		return listaImpCosladaPeriodico;
	}

	public void guardarListaExpPeriodico() {
		try {
			ObjectOutputStream guardarListaExpPeriodico = new ObjectOutputStream(new FileOutputStream("LasTablas-ListaExpPeriodico.dat"));
			guardarListaExpPeriodico.writeObject(listaExpPeriodico);
			guardarListaExpPeriodico.close();
		} catch (Exception e) {
			System.out.println("No fue posible guardar listaExpPeriodico");
		}
	}

	public void anadirExpPeriodico(Periodico periodico) {
		listaExpPeriodico.add(periodico);
		guardarListaExpPeriodico();
	}
	
	public void cargarListaImpMostolesPeriodico() {
	    try {
			ObjectInputStream cargarListaImpMostolesPeriodico = new ObjectInputStream(new FileInputStream("Mostoles-ListaPeriodico.dat"));
			listaImpMostolesPeriodico = (List<Periodico>) cargarListaImpMostolesPeriodico.readObject();
			gestorRedPeriodico.setListaImpMostolesPeriodico(listaImpMostolesPeriodico);
			cargarListaImpMostolesPeriodico.close();
		} catch (Exception e) {
			System.out.println("Error en la lectura de periodicos de Mostoles");
		}
	}
	
	public void cargarListaImpCosladaPeriodico() {
	    try {
			ObjectInputStream cargarListaImpCosladaPeriodico = new ObjectInputStream(new FileInputStream("Coslada-ListaPeriodico.dat"));
			listaImpCosladaPeriodico = (List<Periodico>) cargarListaImpCosladaPeriodico.readObject();
			gestorRedPeriodico.setListaImpCosladaPeriodico(listaImpCosladaPeriodico);
			cargarListaImpCosladaPeriodico.close();
		} catch (Exception e) {
			System.out.println("Error en la lectura de periodicos de Coslada");
		}
	}
}