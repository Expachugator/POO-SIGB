package red;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class GestorRedRevista {
	
	private List<Revista> listaExpRevista;
	private List<Revista> listaImpMostolesRevista;
	private List<Revista> listaImpCosladaRevista;
	
	public GestorRedRevista() {
		listaExpRevista = new ArrayList<Revista>();
		listaImpMostolesRevista = new ArrayList<Revista>();
		listaImpCosladaRevista = new ArrayList<Revista>();
	}

	public void setListaExpRevista(List<Revista> listaExpRevista) {
		this.listaExpRevista = listaExpRevista;
	}
	
	public void setListaImpMostolesRevista(List<Revista> listaImpMostolesRevista) {
		this.listaImpMostolesRevista = listaImpMostolesRevista;
	}

    public void setListaImpCosladaRevista(List<Revista> listaImpCosladaRevista) {
		this.listaImpCosladaRevista = listaImpCosladaRevista;
	}

	public List<Revista> getListaExpRevista() {
		return listaExpRevista;
	}
	
	public List<Revista> getListaImpMostolesRevista() {
		return listaImpMostolesRevista;
	}

    public List<Revista> getListaImpCosladaRevista() {
		return listaImpCosladaRevista;
	}

	public void guardarListaExpRevista() {
		try {
			ObjectOutputStream guardarListaExpRevista = new ObjectOutputStream(new FileOutputStream("LasTablas-ListaExpRevista.dat"));
			guardarListaExpRevista.writeObject(listaExpRevista);
			guardarListaExpRevista.close();
		} catch (Exception e) {
			System.out.println("No fue posible guardar listaExpRevista");
		}
	}

	public void anadirExpPeriodico(Revista revista) {
		listaExpRevista.add(revista);
		guardarListaExpRevista();
	}
	
	public void cargarListaImpMostolesRevista() {
	    try {
			ObjectInputStream cargarListaImpMostolesRevista = new ObjectInputStream(new FileInputStream("Mostoles-ListaRevista.dat"));
			listaImpMostolesRevista = (List<Revista>) cargarListaImpMostolesRevista.readObject();
			gestorRedRevista.setListaImpMostolesRevista(listaImpMostolesRevista);
			cargarListaImpMostolesRevista.close();
		} catch (Exception e) {
			System.out.println("Error en la lectura de revistas de Mostoles");
		}
	}
	
	public void cargarListaImpCosladaRevista() {
	    try {
			ObjectInputStream cargarListaImpCosladaRevista = new ObjectInputStream(new FileInputStream("Coslada-ListaRevista.dat"));
			listaImpCosladaRevista = (List<Revista>) cargarListaImpCosladaRevista.readObject();
			gestorRedRevista.setListaImpCosladaRevista(listaImpCosladaRevista);
			cargarListaImpCosladaRevista.close();
		} catch (Exception e) {
			System.out.println("Error en la lectura de revistas de Coslada");
		}
	}
}