package red;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class GestorRedLibro {
	
	private List<Libro> listaExpLibro;
	private List<Libro> listaImpMostolesLibro;
	private List<Libro> listaImpCosladaLibro;

	public GestorRedLibro() {
		listaExpLibro = new ArrayList<Libro>();
		listaImpMostolesLibro = new ArrayList<Libro>();
		listaImpCosladaLibro = new ArrayList<Libro>();
	}

	public void setListaExpLibro(List<Libro> listaExpLibro) {
		this.listaExpLibro = listaExpLibro;
	}
	
	public void setListaImpMostolesLibro(List<Libro> listaImpMostolesLibro) {
		this.listaImpMostolesLibro = listaImpMostolesLibro;
	}
	
	public void setListaImpCosladaLibro(List<Libro> listaImpCosladaLibro) {
		this.listaImpCosladaLibro = listaImpCosladaLibro;
	}

	public List<Libro> getListaExpLibro() {
		return listaExpLibro;
	}
	
	public List<Libro> getListaImpMostolesLibro() {
		return listaImpMostolesLibro;
	}
	
	public List<Libro> getListaImpCosladaLibro() {
		return listaImpCosladaLibro;
	}

	public void guardarListaExpLibro() {
		try {
			ObjectOutputStream guardarListaExpLibro = new ObjectOutputStream(new FileOutputStream("LasTablas-ListaExpLibro.dat"));
			guardarListaExpLibro.writeObject(listaExpLibro);
			guardarListaExpLibro.close();
		} catch (Exception e) {
			System.out.println("No fue posible guardar listaExpLibro");
		}
	}

	public void anadirExpLibro(Libro Libro) {
		listaExpLibro.add(libro);
		guardarListaExpLibro();
	}
	
	public void cargarListaImpMostolesLibro() {
	    try {
			ObjectInputStream cargarListaImpMostolesLibro = new ObjectInputStream(new FileInputStream("Mostoles-ListaLibro.dat"));
			listaImpMostolesLibro = (List<Libro>) cargarListaImpMostolesLibro.readObject();
			gestorRedLibro.setListaImpMostolesLibro(listaImpMostolesLibro);
			cargarListaImpMostolesLibro.close();
		} catch (Exception e) {
			System.out.println("Error en la lectura de libros de Mostoles");
		}
	}
	
	public void cargarListaImpCosladaLibro() {
	    try {
			ObjectInputStream cargarListaImpCosladaLibro = new ObjectInputStream(new FileInputStream("Coslada-ListaLibro.dat"));
			listaImpCosladaLibro = (List<Libro>) cargarListaImpCosladaLibro.readObject();
			gestorRedLibro.setListaImpCosladaLibro(listaImpCosladaLibro);
			cargarListaImpCosladaLibro.close();
		} catch (Exception e) {
			System.out.println("Error en la lectura de libros de Coslada");
		}
	}
}
