package red;

public class RedRevista extends RedMaterial {

	private final String numero;
	private final String mes;
	private final String ano;
	private final String genero;

	public String getGenero() {
		return genero;
	}

	public String getNumero() {
		return numero;
	}

	public String getMes() {
		return mes;
	}

	public String getAno() {
		return ano;
	}

	public RedRevista(String titulo, String genero, String numero, String mes, String ano, int idMat, String "Las Tablas") {
		super (idMat, titulo);
		this.genero = genero;
		this.numero = numero;
		this.mes = mes;
		this.ano = ano;
	}
}
