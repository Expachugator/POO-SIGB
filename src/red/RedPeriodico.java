package red;

public class RedPeriodico extends RedMaterial {

	private final String dia;
	private final String mes;
	private final String ano;

	public String getDia() {
		return dia;
	}

	public String getMes() {
		return mes;
	}

	public String getAno() {
		return ano;
	}

	public RedPeriodico(String titulo, String dia, String mes, String ano, int idMat, String "Las Tablas") {
		super (titulo, idMat);
		this.dia = dia;
		this.mes = mes;
		this.ano = ano;
	}
}
