package prestamos;

import java.io.Serializable;

//Clase con los parametros correspondientes a los prestamos

public class Prestamo implements Serializable {

	int idMat;
	Boolean prestado;
	String tipoMat;
	String titulo;
	int idUsu;
	public String fechaPrestamo;
	public String fechaDevolucion;

	public String getFechaPrestamo() {
		return fechaPrestamo;
	}

	public String getFechaDevolucion() {
		return fechaDevolucion;
	}

	public int getIdMat() {
		return idMat;
	}

	public Boolean getPrestado() {
		return prestado;
	}

	public int getIdUsu() {
		return idUsu;
	}

	public String getTipoMat() {
		return tipoMat;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setPrestado(Boolean prestado) {
		this.prestado = prestado;
	}

	public void setFechaDevolucion(String fechaDevolucion) {
		this.fechaDevolucion = fechaDevolucion;
	}

	public Prestamo(int idMat, boolean prestado, String tipoMat, String titulo, int idUsu, String fechaAlta,
			String fechaDevolucion) {
		this.idMat = idMat;
		this.prestado = prestado;
		this.tipoMat = tipoMat;
		this.titulo = titulo;
		this.idUsu = idUsu;
		this.fechaPrestamo = fechaAlta;
		this.fechaDevolucion = fechaDevolucion;
	}

	public Prestamo(int idMat, Boolean prestado, String tipoMat, String titulo) {
		this.idMat = idMat;
		this.prestado = prestado;
		this.tipoMat = tipoMat;
		this.titulo = titulo;
	}
}