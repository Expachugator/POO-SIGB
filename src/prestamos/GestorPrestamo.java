package prestamos;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

//Clase con los getter y setter que hacen referencia a los prestamos, asi como las principales acciones

public class GestorPrestamo {

	private List<Prestamo> listaPrestamo;

	public GestorPrestamo() {
		listaPrestamo = new ArrayList<Prestamo>();
	}

	public void setListaPrestamo(List<Prestamo> listaPrestamo) {
		this.listaPrestamo = listaPrestamo;
	}

	public List<Prestamo> getListaPrestamo() {
		return listaPrestamo;
	}

	public void guardarPrestamo() {
		try {
			ObjectOutputStream guardarListaPrestamo = new ObjectOutputStream(new FileOutputStream("ListaPrestamo.dat"));
			guardarListaPrestamo.writeObject(listaPrestamo);
			guardarListaPrestamo.close();
		} catch (Exception e) {
			System.out.println("No fue posible guardar listaPrestamoLibro");
		}
	}

	public void nuevoPrestamo(Prestamo prestamoDatos) {
		listaPrestamo.add(prestamoDatos);
		guardarPrestamo();
	}
}
