package main;

//Se importan los distintos gestores
import materiales.GestorLibro;
import materiales.GestorPeriodico;
import materiales.GestorRevista;
import usuarios.GestorUsuario;
import usuarios.GestorBibliotecario;
import prestamos.GestorPrestamo;
import red.GestorRedLibro;
import red.GestorRedPeriodico;
import red.GestorRedRevista;
import suscripciones.GestorSuscripcion;

public class FactoriaGestores {

    //Se crean los distintos gestores con la clase Gestor correspondiente
	private static GestorLibro gestorLibro;
	private static GestorPeriodico gestorPeriodico;
	private static GestorRevista gestorRevista;
	private static GestorUsuario gestorUsuario;
	private static GestorBibliotecario gestorBibliotecario;
	private static GestorPrestamo gestorPrestamo;
	private static GestorSuscripcion gestorSuscripcion;
	private static GestorRedLibro gestorRedLibro;
	private static GestorRedPeriodico gestorRedPeriodico;
	private static GestorRedRevista gestorRedRevista;
	
    //Cada vez que se llama a una de las funciones crear se comprueba si existe el gestor correspondiente, si existe se devuelve, si no existe se crea y se devuelve
	public static GestorLibro crearGestorLibro() {
		if (gestorLibro == null) {
			gestorLibro = new GestorLibro();
		}
		return gestorLibro;
	}

	public static GestorPeriodico crearGestorPeriodico() {
		if (gestorPeriodico == null) {
			gestorPeriodico = new GestorPeriodico();
		}
		return gestorPeriodico;
	}

	public static GestorRevista crearGestorRevista() {
		if (gestorRevista == null) {
			gestorRevista = new GestorRevista();
		}
		return gestorRevista;
	}

	public static GestorUsuario crearGestorUsuario() {
		if (gestorUsuario == null) {
			gestorUsuario = new GestorUsuario();
		}
		return gestorUsuario;
	}

	public static GestorBibliotecario crearGestorBibliotecario() {
		if (gestorBibliotecario == null) {
			gestorBibliotecario = new GestorBibliotecario();
		}
		return gestorBibliotecario;
	}

	public static GestorPrestamo crearGestorPrestamo() {
		if (gestorPrestamo == null) {
			gestorPrestamo = new GestorPrestamo();
		}
		return gestorPrestamo;
	}

	public static GestorSuscripcion crearGestorSuscripcion() {
		if (gestorSuscripcion == null) {
			gestorSuscripcion = new GestorSuscripcion();
		}
		return gestorSuscripcion;
	}
	
	public static GestorRedLibro crearGestorRedLibro() {
		if (gestorRedLibro == null) {
			gestorRedLibro = new GestorRedLibro();
		}
		return gestorRedLibro;
	}
	
	public static GestorRedPeriodico crearGestorRedPeriodico() {
		if (gestorRedPeriodico == null) {
			gestorRedPeriodico = new GestorRedPeriodico();
		}
		return gestorRedPeriodico;
	}
	
	public static GestorRedRevista crearGestorRedRevista() {
		if (gestorRedRevista == null) {
			gestorRedRevista = new GestorRedRevista();
		}
		return gestorRedRevista;
	}
}