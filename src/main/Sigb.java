package main;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;
import materiales.*;
import menusBiblio.MenuBiblio;
import prestamos.*;
import suscripciones.*;
import usuarios.*;

//Esta es la clase principal del proyecto

public class Sigb {

	//Llamada a la FactoriaGestores para hacer referencia siempre a los mismos gestores
	
	private static GestorLibro gestorLibro = FactoriaGestores.crearGestorLibro();
	private static GestorPeriodico gestorPeriodico = FactoriaGestores.crearGestorPeriodico();
	private static GestorRevista gestorRevista = FactoriaGestores.crearGestorRevista();
	private static GestorUsuario gestorUsuario = FactoriaGestores.crearGestorUsuario();
	private static GestorBibliotecario gestorBibliotecario = FactoriaGestores.crearGestorBibliotecario();
	private static GestorPrestamo gestorPrestamo = FactoriaGestores.crearGestorPrestamo();
	private static GestorSuscripcion gestorSuscripcion = FactoriaGestores.crearGestorSuscripcion();

	public static void main(String[] args) {
		cargarDatos();				//Primero cargamos los datos de usuarios, materiales y suscripciones
		comprobarSanciones();		//Comprobamos si alguna suscripcion a expirado para anularla
		imprimirInicio();			//Vamos a la pantalla de inicio para ingresar en la aplicacion
	}

	public static void imprimirInicio() {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Bienvenido a la biblioteca");
		System.out.println("Introduzca sus datos");
		System.out.println("Usuario: ");
		String loginUsuario = scanner.next();
		System.out.println("Contraseña: ");
		String loginContrasena = scanner.next();
		//Primero se comprueba si se han introducido los datos del bibliotecario MASTER
		if (loginUsuario.equals("MASTER") && loginContrasena.equals("ADMIN")) {
			MenuBiblio.menuBiblio();
		} else { //Si no se han introducido se comprueba el usuario
			ComprobarUsuario.comprobarBibliotecario(loginUsuario, loginContrasena);
		}
	}

	private static void cargarDatos() {

        //Para cargar los datos se crean las listas del tipo ArrayList correspondientes
		List<Bibliotecario> listaBibliotecario;
		listaBibliotecario = new ArrayList<Bibliotecario>();

		List<Usuario> listaUsuario;
		listaUsuario = new ArrayList<Usuario>();

		List<Libro> listaLibro;
		listaLibro = new ArrayList<Libro>();

		List<Periodico> listaPeriodico;
		listaPeriodico = new ArrayList<Periodico>();

		List<Revista> listaRevista;
		listaRevista = new ArrayList<Revista>();

		List<Prestamo> listaPrestamo;
		listaPrestamo = new ArrayList<Prestamo>();

		List<Suscripcion> listaSuscripcion;
		listaSuscripcion = new ArrayList<Suscripcion>();

        //Se completan con los ficheros que hemos guardado anteriormente
		try {
			ObjectInputStream cargarListaBibliotecario = new ObjectInputStream(
					new FileInputStream("ListaBibliotecario.dat"));
			listaBibliotecario = (List<Bibliotecario>) cargarListaBibliotecario.readObject();
			gestorBibliotecario.setListaBibliotecario(listaBibliotecario);
			cargarListaBibliotecario.close();
		} catch (Exception e) {
			System.out.println("Error en la lectura de bibliotecarios");
		}

		try {
			ObjectInputStream cargarListaUsuario = new ObjectInputStream(new FileInputStream("ListaUsuario.dat"));
			listaUsuario = (List<Usuario>) cargarListaUsuario.readObject();
			gestorUsuario.setListaUsuario(listaUsuario);
			cargarListaUsuario.close();
		} catch (Exception e) {
			System.out.println("Error en la lectura de usuarios");
		}

		try {
			ObjectInputStream cargarListaLibro = new ObjectInputStream(new FileInputStream("ListaLibro.dat"));
			listaLibro = (List<Libro>) cargarListaLibro.readObject();
			gestorLibro.setListaLibro(listaLibro);
			cargarListaLibro.close();
		} catch (Exception e) {
			System.out.println("Error en la lectura de libros");
		}

		try {
			ObjectInputStream cargarListaPeriodico = new ObjectInputStream(new FileInputStream("ListaPeriodico.dat"));
			listaPeriodico = (List<Periodico>) cargarListaPeriodico.readObject();
			gestorPeriodico.setListaPeriodico(listaPeriodico);
			cargarListaPeriodico.close();
		} catch (Exception e) {
			System.out.println("Error en la lectura de periodicos");
		}

		try {
			ObjectInputStream cargarListaRevista = new ObjectInputStream(new FileInputStream("ListaRevista.dat"));
			listaRevista = (List<Revista>) cargarListaRevista.readObject();
			gestorRevista.setListaRevista(listaRevista);
			cargarListaRevista.close();
		} catch (Exception e) {
			System.out.println("Error en la lectura de revistas");
		}

		try {
			ObjectInputStream cargarListaPrestamo = new ObjectInputStream(new FileInputStream("ListaPrestamo.dat"));
			listaPrestamo = (List<Prestamo>) cargarListaPrestamo.readObject();
			gestorPrestamo.setListaPrestamo(listaPrestamo);
			cargarListaPrestamo.close();
		} catch (Exception e) {
			System.out.println("Error en la lectura de prestamos");
		}

		try {
			ObjectInputStream cargarListaSuscripcion = new ObjectInputStream(
					new FileInputStream("ListaSuscripcion.dat"));
			listaSuscripcion = (List<Suscripcion>) cargarListaSuscripcion.readObject();
			gestorSuscripcion.setListaSuscripcion(listaSuscripcion);
			cargarListaSuscripcion.close();
		} catch (Exception e) {
			System.out.println("Error en la lectura de suscripciones");
		}
	}

	private static void comprobarSanciones() {
	    //Se calcula que dia es hoy y se guarda
		String fechaHoy;
		Date fecha = new Date();
		SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
		fechaHoy = formatoFecha.format(fecha);

        //Se inicializa la lista de usuarios
		Iterator<Usuario> it = gestorUsuario.getListaUsuario().iterator();

        //Se mira y guarda en distintas variables el dia, mes y año que es hoy
		String anoHoy = fechaHoy.substring(6, 10);
		int anoHoyInt = Integer.parseInt(anoHoy);
		String mesHoy = fechaHoy.substring(3, 5);
		int mesHoyInt = Integer.parseInt(mesHoy);
		String diaHoy = fechaHoy.substring(0, 2);
		int diaHoyInt = Integer.parseInt(diaHoy);

        //Mientras haya usuarios se estara en el bucle
		while (it.hasNext()) {
			Usuario i = it.next();
			//Si el usuario i esta sancionado se comprueba el año mes y dia, si ha pasado la fecha de sancion se le quita
			if (i.getSancion() != null) {
				String anoSan = i.getSancion().substring(6, 10);
				int anoSanInt = Integer.parseInt(anoSan);
				if (anoSanInt < anoHoyInt) {
					i.setSancion(null);
					gestorUsuario.getListaUsuario();
				} else if (anoSanInt == anoHoyInt) {
					String mesSan = i.getSancion().substring(3, 5);
					int mesSanInt = Integer.parseInt(mesSan);
					if (mesSanInt < mesHoyInt) {
						i.setSancion(null);
						gestorUsuario.getListaUsuario();
					} else if (mesSanInt == mesHoyInt) {
						String diaSan = i.getSancion().substring(0, 2);
						int diaSanInt = Integer.parseInt(diaSan);
						if (diaSanInt < diaHoyInt) {
							i.setSancion(null);
							gestorUsuario.guardarListaUsuario();
						}
					}
				}
			}
		}
	}
}