package usuarios;

import java.io.*;
import java.util.*;

//Clase con los getter y setter que hacen referencia a los bibliotecarios, asi como las principales acciones

public class GestorBibliotecario{

	private List<Bibliotecario> listaBibliotecario;

	public GestorBibliotecario() {
		listaBibliotecario = new ArrayList<Bibliotecario>();
	}

	public void setListaBibliotecario(List<Bibliotecario> listaBibliotecario) {
		this.listaBibliotecario = listaBibliotecario;
	}

	public List<Bibliotecario> getListaBibliotecario() {
		return listaBibliotecario;
	}

	public void guardarListaBibliotecario() {
		try {
			ObjectOutputStream guardarListaBibliotecario = new ObjectOutputStream(
					new FileOutputStream("ListaBibliotecario.dat"));
			guardarListaBibliotecario.writeObject(listaBibliotecario);
			guardarListaBibliotecario.close();
		} catch (Exception e) {
			System.out.println("No fue posible guardar listaBibliotecario");
		}
	}

	public void anadirBibliotecario(Bibliotecario bibliotecario) {
		listaBibliotecario.add(bibliotecario);
		guardarListaBibliotecario();
	}

	public void borrarBibliotecario(Bibliotecario bibliotecario) {
		listaBibliotecario.remove(bibliotecario);
		guardarListaBibliotecario();
	}
}
