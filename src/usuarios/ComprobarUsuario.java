package usuarios;

import main.FactoriaGestores;
import main.Sigb;
import menusBiblio.MenuBiblio;
import menusUsu.MenuUsu;
import java.util.Iterator;

public class ComprobarUsuario {

    //Se traen los gestores unicos
	private static GestorUsuario gestorUsuario = FactoriaGestores.crearGestorUsuario();
	private static GestorBibliotecario gestorBibliotecario = FactoriaGestores.crearGestorBibliotecario();

    //Se instancia la lista de bibliotecarios y si algun dni coincide con el que se introdujo se comprueba la contraseña
	public static void comprobarBibliotecario(String loginUsuario, String loginContrasena) {
		Iterator<Bibliotecario> it = gestorBibliotecario.getListaBibliotecario().iterator();
		while (it.hasNext()) {
			Bibliotecario u = it.next();
			if (u.getDni().equals(loginUsuario)) {
				contrasenaBibliotecario(u, loginContrasena);
			}
		}
		//Si no ha coincidido se comprueba con los usuarios
		comprobarUsuario(loginUsuario, loginContrasena);
	}

    //Se instancia la lista de usuarios y si algun dni coincide con el que se introdujo se comprueba la contraseña
	public static void comprobarUsuario(String loginUsuario, String loginContrasena) {
		Iterator<Usuario> it = gestorUsuario.getListaUsuario().iterator();
		while (it.hasNext()) {
			Usuario u = it.next();
			if (u.getDni().equals(loginUsuario)) {
				contrasenaUsuario(u, loginContrasena);
			}
		}
		//Si no ha coincidido se muestra el error y se vuelve a la pantalla anterior
		System.out.println("Usuario y/o contraseña incorrectos, intentelo de nuevo");
		Sigb.imprimirInicio();
	}

    //Comprueba la contraseña del usuario
	public static void contrasenaUsuario(Usuario u, String loginContrasena) {
		if (u.getContrasena().equals(loginContrasena)) {
			MenuUsu.menuUsu(u);
		}
	}

    //Comprueba la contraseña del bibliotecario
	public static void contrasenaBibliotecario(Bibliotecario u, String loginContrasena) {
		if (u.getContrasena().equals(loginContrasena)) {
			MenuBiblio.menuBiblio();
		}
	}
}
