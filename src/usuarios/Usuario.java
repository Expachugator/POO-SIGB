package usuarios;

//Clase usuario con los getter y setter
public final class Usuario extends TipoUsuario{

	int prestamos;
	String sancion;

	public int getPrestamos() {
		return prestamos;
	}

	public String getSancion() {
		return sancion;
	}

	public void setPrestamos(int prestamos) {
		this.prestamos = prestamos;
	}

	public void setSancion(String sancion) {
		this.sancion = sancion;
	}

	public Usuario(String nombre, String apellido, String contrasena, String dni, int prestamos, String sancion) {
		super(nombre, apellido, contrasena, dni);
	}
}
