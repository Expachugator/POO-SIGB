package usuarios;

public final class Bibliotecario extends TipoUsuario {

	public Bibliotecario(String nombre, String apellido, String contrasena, String dni) {
		super(nombre, apellido, contrasena, dni);
	}
}
