package usuarios;

import java.io.*;
import java.util.*;

//Clase con los getter y setter que hacen referencia a los usuarios, asi como las principales acciones

public class GestorUsuario{

	private List<Usuario> listaUsuario;

	public GestorUsuario() {
		listaUsuario = new ArrayList<Usuario>();
	}
	
	public void setListaUsuario(List<Usuario> listaUsuario) {
		this.listaUsuario = listaUsuario;
	}

	public List<Usuario> getListaUsuario() {
		return listaUsuario;
	}

	public void guardarListaUsuario(){
		try {
			ObjectOutputStream guardarListaUsuario = new ObjectOutputStream(new FileOutputStream("listaUsuario.dat"));
			guardarListaUsuario.writeObject(listaUsuario);
			guardarListaUsuario.close();
		} catch (Exception e) {
			System.out.println("No fue posible guardar listaUsuario");
		}
	}

	public void anadirUsuario(Usuario usuario) {
		listaUsuario.add(usuario);
		guardarListaUsuario();
	}
	
	public void borrarUsuario(Usuario usuario) {
		listaUsuario.remove(usuario);
		guardarListaUsuario();
	}
}
