package usuarios;

import java.io.Serializable;
import java.util.Iterator;

import main.FactoriaGestores;

//Clase con los parametros comunes a todos los usuarios y bibliotecarios

public abstract class TipoUsuario implements Serializable{
	
	private static GestorBibliotecario gestorBibliotecario = FactoriaGestores.crearGestorBibliotecario();
	private static GestorUsuario gestorUsuario = FactoriaGestores.crearGestorUsuario();
	private final int idUsuario;
	
	String nombre;
	String apellidos;
	String contrasena;
	String dni;
	
	public String getNombre(){
		return nombre;
	}
	
	public String getApellidos(){
		return apellidos;
	}
	
	public String getContrasena(){
		return contrasena;
	}
	
	public String getDni(){
		return dni;
	}
	
	public int getIdUsuario(){
		return idUsuario;
	}
	
	public TipoUsuario(String nombre, String apellidos, String contrasena, String dni){
	//Se crea la variable idGlobal que obendra el id asignado a el ultimo tipo de usuario y una de cada tipo de material
		int idGlobal = 0;
		int idGlobalBiblio = 0;
		int idGlobalUsu = 0;
		Iterator<Bibliotecario> itBiblio = gestorBibliotecario.getListaBibliotecario().iterator();
		while (itBiblio.hasNext()) {
			Bibliotecario i = itBiblio.next();
			idGlobalBiblio = i.getIdUsuario();
		}
		Iterator<Usuario> itUsu = gestorUsuario.getListaUsuario().iterator();
		while (itUsu.hasNext()) {
			Usuario i = itUsu.next();
			idGlobalUsu = i.getIdUsuario();
		}
		//Se asigna a idGlobal el id que sea mayor de cada uno de los distintos materiales
		if (idGlobalBiblio > idGlobal){
			idGlobal = idGlobalBiblio;
		} 
		if (idGlobalUsu > idGlobal){
			idGlobal = idGlobalUsu;
		}

		this.nombre = nombre;
		this.apellidos = apellidos;
		this.contrasena = contrasena;
		this.dni = dni;
		this.idUsuario = idGlobal + 1;
		System.out.println("Id asignado: " + this.idUsuario);
	}

}
