package menusBiblio;

import java.util.*;

//Clase con el menuUsuMat que nos da acceso a otras funciones
public class MenuBiblioPresList {

	public static void menuBiblioPresList() {

		Scanner scanner = new Scanner(System.in);

		System.out.println("Elija de que material quiere el listado:");
		System.out.println("1 - Libros");
		System.out.println("2 - Periodicos");
		System.out.println("3 - Revistas");
		System.out.println("4 - Todo");
		System.out.println("0 - Atras");

    //try para evitar que se introduzcan datos de tipo distinto a int
		try {
			int opcion = scanner.nextInt();

			switch (opcion) {
			case 1:
				MenuBiblioPresListLibro.menuBiblioPresListLibro();
				break;
			case 2:
				MenuBiblioPresListPeriodico.menuBiblioPresListPeriodico();
				break;
			case 3:
				MenuBiblioPresListRevista.menuBiblioPresListRevista();
				break;
			case 4:
				MenuBiblioPresListTodo.menuBiblioPresListTodo();
				break;
			case 0:
				MenuBiblioPres.menuBiblioPres();
				break;
			default:
				System.out.println("Introduzca una opcion correcta");
				MenuBiblioPresList.menuBiblioPresList();
				break;
			}
		} catch (Exception e) {
			System.out.println("Introduza una opcion correcta");
			MenuBiblioPresList.menuBiblioPresList();
		}
	}
}