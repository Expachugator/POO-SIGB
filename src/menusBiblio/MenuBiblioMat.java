package menusBiblio;

import java.util.*;

//Clase con el menuUsuMat que nos da acceso a otras funciones
public class MenuBiblioMat {

	public static void menuBiblioMat() {

		Scanner scanner = new Scanner(System.in);

		System.out.println("Seleccione una opcion");
		System.out.println("1 - Nuevo material");
		System.out.println("2 - Borrar material");
		System.out.println("3 - Busqueda material");
		System.out.println("4 - Listado material");
		System.out.println("0 - Atras");

    //try para evitar que se introduzcan datos de tipo distinto a int
		try {
			int opcion = scanner.nextInt();

			switch (opcion) {
			case 1:
				MenuBiblioMatNuevo.menuBiblioMatNuevo();
				break;
			case 2:
				MenuBiblioMatBorrar.menuBiblioMatBorrar();
				break;
			case 3:
				MenuBiblioMatBusqueda.menuBiblioMatBusqueda();
				break;
			case 4:
				MenuBiblioMatListado.menuBiblioMatListado();
				break;
			case 0:
				MenuBiblio.menuBiblio();
				break;
			default:
				System.out.println("Introduzca una opcion correcta");
				MenuBiblioMat.menuBiblioMat();
				break;
			}
		} catch (Exception e) {
			System.out.println("Introduza una opcion correcta");
			MenuBiblioMat.menuBiblioMat();
		}
	}
}