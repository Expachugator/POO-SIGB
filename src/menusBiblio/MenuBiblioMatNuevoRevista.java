package menusBiblio;

import java.util.*;

import main.FactoriaGestores;
import materiales.*;

public class MenuBiblioMatNuevoRevista {

    //Devuelve el gestor de revistas
	private static GestorRevista gestorRevista = FactoriaGestores.crearGestorRevista();

	public static void menuBiblioMatNuevoRevista() {

		Scanner scanner = new Scanner(System.in);

    //Pide los datos de la revista
		System.out.println("Titulo: ");
		String titulo = scanner.nextLine();
		System.out.println("Genero: ");
		String genero = scanner.nextLine();
		System.out.println("Numero: ");
		String numero = scanner.nextLine();
		System.out.println("Mes: ");
		String mes = scanner.nextLine();
		System.out.println("Año: ");
		String ano = scanner.nextLine();
		Boolean prestado = false;

    //los guarda en la variable revista del tipo Revista
		Revista revista = new Revista(titulo, genero, prestado, numero, mes, ano);

    //guarda el libro en la lista de revistas
		gestorRevista.anadirRevista(revista);

		System.out.println("Revista creada con IdMat " + revista.getIdMat() + " no olvide etiquetarla");

		MenuBiblioMatNuevo.menuBiblioMatNuevo();
	}
}
