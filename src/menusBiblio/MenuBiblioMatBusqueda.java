package menusBiblio;

import java.util.*;

//Clase con el menuUsuMat que nos da acceso a otras funciones
public class MenuBiblioMatBusqueda {

	public static void menuBiblioMatBusqueda() {

		Scanner scanner = new Scanner(System.in);

		System.out.println("Seleccione el tipo de material que quiere buscar:\n");
		System.out.println("1 - Libro");
		System.out.println("2 - Periodico");
		System.out.println("3 - Revista");
		System.out.println("4 - Todos");
		System.out.println("0 - Atras");

    //try para evitar que se introduzcan datos de tipo distinto a int
		try {
			int opcion = scanner.nextInt();

			switch (opcion) {
			case 1:
				MenuBiblioMatBusquedaLibro.menuBiblioMatBusquedaLibro();
				break;
			case 2:
				MenuBiblioMatBusquedaPeriodico.menuBiblioMatBusquedaPeriodico();
				break;
			case 3:
				MenuBiblioMatBusquedaRevista.menuBiblioMatBusquedaRevista();
				break;
			case 4:
				MenuBiblioMatBusquedaTodo.menuBiblioMatBusquedaTodo();
				break;
			case 0:
				MenuBiblioMat.menuBiblioMat();
				break;
			default:
				System.out.println("Introduzca una opcion correcta");
				MenuBiblioMatBusqueda.menuBiblioMatBusqueda();
				break;
			}
		} catch (Exception e) {
			System.out.println("Introduza una opcion correcta");
			MenuBiblioMatBusqueda.menuBiblioMatBusqueda();
		}
	}
}