package menusBiblio;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
import main.FactoriaGestores;
import suscripciones.GestorSuscripcion;
import suscripciones.Suscripcion;

public class MenuBiblioSusNuevaPeriodico {

    //devuelve el gestor
    private static GestorSuscripcion gestorSuscripcion = FactoriaGestores.crearGestorSuscripcion();

	public static void menuBiblioSusNuevaPeriodico(){
		
		//calcula el dia de hoy
    	String fechaInicio, fechaFin;
		Date fecha = new Date();
		SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
		fechaInicio = formatoFecha.format(fecha);
		fechaFin = null;
			
		Scanner scanner = new Scanner(System.in);
		
		//pide los datos
		System.out.println("Complete los siguientes campos:");
		System.out.println("Titulo");
		String titulo = scanner.nextLine();
		System.out.println("Tipo de suscripcion (mensual, semanal, diaria)");
		String tipo = scanner.nextLine();
		
		//guarda los datos de suscripcion y los guarda en la lista
		Suscripcion datosSuscripcion = new Suscripcion("Periodico", titulo, tipo, fechaInicio, fechaFin);
        gestorSuscripcion.anadirSuscripcion(datosSuscripcion);

		System.out.println("Suscrito con exito al periodico " + titulo);
        
        MenuBiblioSusNueva.menuBiblioSusNueva();
								
	}
}