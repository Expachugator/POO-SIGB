package menusBiblio;

import java.text.SimpleDateFormat;
import java.util.*;

import main.FactoriaGestores;
import materiales.GestorLibro;
import materiales.GestorPeriodico;
import materiales.GestorRevista;
import materiales.Libro;
import materiales.Periodico;
import materiales.Revista;
import prestamos.GestorPrestamo;
import prestamos.Prestamo;
import usuarios.GestorUsuario;
import usuarios.Usuario;

public class MenuBiblioPresDev {

    //devuelve los gestores
	private static GestorUsuario gestorUsuario = FactoriaGestores.crearGestorUsuario();
	private static GestorLibro gestorLibro = FactoriaGestores.crearGestorLibro();
	private static GestorPeriodico gestorPeriodico = FactoriaGestores.crearGestorPeriodico();
	private static GestorRevista gestorRevista = FactoriaGestores.crearGestorRevista();
	private static GestorPrestamo gestorPrestamo = FactoriaGestores.crearGestorPrestamo();

	public static void menuBiblioPresDev() {

    //calcula que fecha es hoy y la guarda
		String fechaHoy;
		Date fecha = new Date();
		SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
		fechaHoy = formatoFecha.format(fecha);

		Scanner scanner = new Scanner(System.in);

		System.out.println("Indique el numero idMat del material a devolver: ");

    //pide el material a devolver
		int material = scanner.nextInt();

		buscarMaterialLibro(material, fechaHoy); //Tras introducir el idMat se procede a buscar

	}

	private static void buscarMaterialLibro(int material, String fechaHoy) {
		boolean encontrado = false;
		//inicializa la lista de libros
		Iterator<Libro> it = gestorLibro.getListaLibro().iterator();
		while (it.hasNext()) {
			Libro i = it.next();
			if (i.getIdMat() == material) { //si se encuentra entre los libros comprobamos si esta fuera de plazo
				encontrado = true;
				System.out.println("El libro a devolver es: " + i.getTitulo());
				comprobarSancion(material, fechaHoy);
				MenuBiblioPres.menuBiblioPres();
			}
		}
		if (encontrado == false) { //si no esta entre los libros buscamos entre los periodicos
			buscarMaterialPeriodico(material, fechaHoy, encontrado);
		}
	}

	private static void buscarMaterialPeriodico(int material, String fechaHoy, boolean encontrado) {
		Scanner scanner = new Scanner(System.in);
		//inicializa la lista de periodicos
		Iterator<Periodico> it = gestorPeriodico.getListaPeriodico().iterator();
		while (it.hasNext()) {
			Periodico i = it.next();
			if (i.getIdMat() == material) {
				encontrado = true;
				System.out.println("El periodico a devolver es: " + i.getTitulo());
				System.out.println("�Esta seguro de devolver este material? (si/no)");
				String devolver = scanner.nextLine();
				if (devolver.equalsIgnoreCase("no")) {
					MenuBiblioPres.menuBiblioPres();
				} else if (devolver.equalsIgnoreCase("si")) {
					comprobarSancion(material, fechaHoy);
					MenuBiblioPres.menuBiblioPres();
				}
			}
		}
		if (encontrado == false) {
			buscarMaterialRevista(material, fechaHoy); //si tampoco lo encontramos entre las revistas
		}
	}

	private static void buscarMaterialRevista(int material, String fechaHoy) {
		Iterator<Revista> it = gestorRevista.getListaRevista().iterator();
		while (it.hasNext()) {
			Revista i = it.next();
			if (i.getIdMat() == material) {
				System.out.println("La revista a devolver es: " + i.getTitulo());
				comprobarSancion(material, fechaHoy);
				MenuBiblioPres.menuBiblioPres();
			} else {
				System.out.println("El material buscado no esta prestado");
				MenuBiblioPres.menuBiblioPres();
			}
		}
	}

	private static void comprobarSancion(int material, String fechaHoy) {
		Iterator<Prestamo> it = gestorPrestamo.getListaPrestamo().iterator();
		while (it.hasNext()) {
			Prestamo i = it.next();
			if (i.getIdMat() == material) {
				//Extraemos el a�o en que estamos hoy y en que habia que devolverlo y se comprueba si es correcto o no
				String anoDev = i.getFechaDevolucion().substring(6, 10);
				int anoDevInt = Integer.parseInt(anoDev);
				String anoHoy = fechaHoy.substring(6, 10);
				int AnoHoyInt = Integer.parseInt(anoHoy);
				if (anoDevInt > AnoHoyInt) {
					System.out.println("Devolucion correcta");
					actualizarPrestado(i, fechaHoy);
				} else if (anoDevInt < AnoHoyInt) {
					System.out.println("Devolucion fuera de plazo");
					actualizarPrestado(i, fechaHoy);
					asignarSancionAnos(i);
				} else {
					//Extraemos el mes
					String mesDev = i.getFechaDevolucion().substring(3, 5);
					int mesDevInt = Integer.parseInt(mesDev);
					String mesHoy = fechaHoy.substring(3, 5);
					int mesHoyInt = Integer.parseInt(mesHoy);
					if (mesDevInt > mesHoyInt) {
						System.out.println("Devolucion correcta");
						actualizarPrestado(i, fechaHoy);
					} else if (mesDevInt < mesHoyInt) {
						System.out.println("Devolucion fuera de plazo");
						actualizarPrestado(i, fechaHoy);
						asignarSancionMeses(i);
					} else {
						//Extraemos el dia
						String diaDev = i.getFechaDevolucion().substring(0, 2);
						int diaDevInt = Integer.parseInt(diaDev);
						String diaHoy = fechaHoy.substring(0, 2);
						int diaHoyInt = Integer.parseInt(diaHoy);
						if (diaDevInt >= diaHoyInt) {
							System.out.println("Devolucion correcta");
							actualizarPrestado(i, fechaHoy);
						} else {
							System.out.println("Devolucion fuera de plazo");
							actualizarPrestado(i, fechaHoy);
							asignarSancionDias(i);
						}
					}
				}
			}
		}
	}

	private static void asignarSancionDias(Prestamo i) {

		//Calculamos 14 dias a partir de hoy para ver hasta cuando es la sancion
		Scanner scanner = new Scanner(System.in);
		Iterator<Usuario> it = gestorUsuario.getListaUsuario().iterator();
		String fechaHoy, fechaSancion;
		Date fecha = new Date();
		SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
		fechaHoy = formatoFecha.format(fecha);
		fecha.setDate(fecha.getDate() + 14);
		fechaSancion = formatoFecha.format(fecha);

		while (it.hasNext()) {
			Usuario u = it.next();
			if (u.getIdUsuario() == i.getIdUsu()) { //Se busca el usuario y se asigna la sancion
				u.setSancion(fechaSancion);
				System.out.println("Asignada sancion al usuario " + u.getNombre() + " de dos semanas hasta el dia "
						+ fechaSancion);
				System.out.println("Pulse intro para continuar...");
				String salir = scanner.nextLine();
				gestorUsuario.guardarListaUsuario();
				MenuBiblioPres.menuBiblioPres();
			}
		}
	}

	private static void asignarSancionMeses(Prestamo i) {
		//Calculamos 60 dias a partir de hoy para ver hasta cuando es la sancion
		Scanner scanner = new Scanner(System.in);
		Iterator<Usuario> it = gestorUsuario.getListaUsuario().iterator();
		String fechaHoy, fechaSancion;
		Date fecha = new Date();
		SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
		fechaHoy = formatoFecha.format(fecha);
		fecha.setDate(fecha.getDate() + 60);
		fechaSancion = formatoFecha.format(fecha);

		while (it.hasNext()) {
			Usuario u = it.next();
			if (u.getIdUsuario() == i.getIdUsu()) {//Se busca el usuario y se asigna la sancion
				u.setSancion(fechaSancion);
				System.out.println(
						"Asignada sancion al usuario " + u.getNombre() + " de dos meses hasta el dia " + fechaSancion);
				System.out.println("Pulse intro para continuar...");
				String salir = scanner.nextLine();
				gestorUsuario.guardarListaUsuario();
				MenuBiblioPres.menuBiblioPres();
			}
		}
	}

	private static void asignarSancionAnos(Prestamo i) {
		//Calculamos 720 dias a partir de hoy para ver hasta cuando es la sancion
		Scanner scanner = new Scanner(System.in);
		Iterator<Usuario> it = gestorUsuario.getListaUsuario().iterator();
		String fechaHoy, fechaSancion;
		Date fecha = new Date();
		SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
		fechaHoy = formatoFecha.format(fecha);
		fecha.setDate(fecha.getDate() + 720);
		fechaSancion = formatoFecha.format(fecha);

		while (it.hasNext()) {
			Usuario u = it.next();
			if (u.getIdUsuario() == i.getIdUsu()) {//Se busca el usuario y se asigna la sancion
				u.setSancion(fechaSancion);
				System.out.println(
						"Asignada sancion al usuario " + u.getNombre() + " de dos años hasta el dia " + fechaSancion);
				System.out.println("Pulse intro para continuar...");
				String salir = scanner.nextLine();
				gestorUsuario.guardarListaUsuario();
				MenuBiblioPres.menuBiblioPres();
			}
		}
	}

	private static void actualizarPrestado(Prestamo i, String fechaHoy) {
		//Al haberse devuelto actualizamos prestado a false
		Iterator<Prestamo> it = gestorPrestamo.getListaPrestamo().iterator();
		Iterator<Libro> itLib = gestorLibro.getListaLibro().iterator();
		Iterator<Periodico> itPer = gestorPeriodico.getListaPeriodico().iterator();
		Iterator<Revista> itRev = gestorRevista.getListaRevista().iterator();

		i.setPrestado(false);
		gestorPrestamo.guardarPrestamo();
		String tipoMat = i.getTipoMat();
		if (tipoMat.equals("Libro")) {
			while (itLib.hasNext()) {
				Libro l = itLib.next();
				if (i.getIdMat() == l.getIdMat()) {
					l.setPrestado(false);
					i.setPrestado(false);
					i.setFechaDevolucion(fechaHoy);
					gestorLibro.guardarListaLibro();
					gestorPrestamo.guardarPrestamo();
				}
			}
		} else if (tipoMat.equals("Periodico")) {
			while (itPer.hasNext()) {
				Periodico p = itPer.next();
				if (i.getIdMat() == p.getIdMat()) {
					p.setPrestado(false);
					i.setPrestado(false);
					i.setFechaDevolucion(fechaHoy);
					gestorPeriodico.guardarListaPeriodico();
					gestorPrestamo.guardarPrestamo();
				}
			}
		} else if (tipoMat.equals("Revista")) {
			while (itRev.hasNext()) {
				Revista r = itRev.next();
				if (i.getIdMat() == r.getIdMat()) {
					r.setPrestado(false);
					i.setPrestado(false);
					i.setFechaDevolucion(fechaHoy);
					gestorRevista.guardarListaRevista();
					gestorPrestamo.guardarPrestamo();
				}
			}
		}
	}
}