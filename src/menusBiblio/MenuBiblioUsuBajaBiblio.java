package menusBiblio;

import java.util.Iterator;
import java.util.Scanner;

import main.FactoriaGestores;
import usuarios.Bibliotecario;
import usuarios.GestorBibliotecario;

public class MenuBiblioUsuBajaBiblio {

    //devuelve el gestor
	private static GestorBibliotecario gestorBibliotecario = FactoriaGestores.crearGestorBibliotecario();

	public static void menuBiblioUsuBajaBiblio() {

		Scanner scanner = new Scanner(System.in);

    //pide el usuario a dar de baja
		System.out.println("Introduzca el IdUsu del bibliotecario a dar de baja:");

    //try para evitar que se introduzcan datos de tipo distinto a int
		try {
			int opcion = scanner.nextInt();
			buscarBibliotecario(opcion);
		} catch (Exception e) {
			System.out.println("Introduzca una opcion valida");
			MenuBiblioUsuBajaUsu.menuBiblioUsuBajaUsu();
		}
	}

	private static void buscarBibliotecario(int opcion) {

		Scanner scanner = new Scanner(System.in);
		//inicializa la lista de bibliotecarios
		Iterator<Bibliotecario> it = gestorBibliotecario.getListaBibliotecario().iterator();

    //busca el usuario y si se confirma lo da de baja
		while (it.hasNext()) {
			Bibliotecario i = it.next();
			if (i.getIdUsuario() == opcion) {
				System.out.println("El usuario seleccionado es el siguiente:");
				System.out.println("Nombre: " + i.getNombre());
				System.out.println("Apellidos: " + i.getApellidos());
				System.out.println("DNI: " + i.getDni());
				System.out.println("¿Seguro quiere dar de baja a este bibliotecario? (si/no)");
				String borrar = scanner.nextLine();
				if (borrar.equals("si")) {
					gestorBibliotecario.borrarBibliotecario(i);
					System.out.println("Bibliotecario dado de baja");
					MenuBiblioUsu.menuBiblioUsu();
				} else if (borrar.equals("no")) {
					MenuBiblioUsu.menuBiblioUsu();
				}
			}
		}
		System.out.println("Usuario no encontrado");
		MenuBiblioUsu.menuBiblioUsu();
	}
}
