package menusBiblio;

import java.util.*;

import main.FactoriaGestores;
import materiales.GestorPeriodico;
import materiales.Periodico;

public class MenuBiblioMatBusquedaPeriodico {

    //Se carga el gestor
	private static GestorPeriodico gestorPeriodico = FactoriaGestores.crearGestorPeriodico();

	public static void menuBiblioMatBusquedaPeriodico() {

		Scanner scanner = new Scanner(System.in);
		Scanner sc = new Scanner(System.in);

		//Se piden los datos por los que se quiere buscar
		System.out.println("Complete los campos en que quiere realizar la busqueda:");
		System.out.println("");
		System.out.println("Titulo: ");
		String busTitulo = scanner.nextLine();
		if (busTitulo.equals("")) {     //Si no se introduce nada dejamos el campo como null
			busTitulo = null;
		}
		System.out.println("Dia: ");
		String busDia = scanner.nextLine();
		if (busDia.equals("")) {        //Si no se introduce nada dejamos el campo como null
			busDia = null;
		}
		System.out.println("Mes: ");
		String busMes = scanner.nextLine();
		if (busMes.equals("")) {        //Si no se introduce nada dejamos el campo como null
			busMes = null;
		}
		System.out.println("Año: ");
		String busAno = scanner.nextLine();
		if (busAno.equals("")) {        //Si no se introduce nada dejamos el campo como null
			busAno = null;
		}

    //Se inicia la lista de periodicos
		Iterator<Periodico> it = gestorPeriodico.getListaPeriodico().iterator();

		System.out.println("Se han encontrado los siguientes periodicos:");

		while (it.hasNext()) {
			Periodico i = it.next();
			
			//Se hace la busqueda mirando si hemos dejado el campo en blanco o si el texto introducido
			//se encuentra entre alguno de los periodicos sin que tenga que ser exacto
			
			if (((busTitulo == null) || (i.getTitulo().toLowerCase().indexOf(busTitulo.toLowerCase())) != -1)
					&& ((busDia == null) || (i.getDia().equals(busDia)))
					&& ((busMes == null) || (i.getMes().equals(busMes)))
					&& ((busAno == null) || (i.getAno().equals(busAno)))) {

				System.out.println("IdMat: " + i.getIdMat());
				System.out.println("Titulo: " + i.getTitulo());
				System.out.println("Dia: " + i.getDia());
				System.out.println("Mes: " + i.getMes());
				System.out.println("Año: " + i.getAno());
			}
		}

		System.out.println("Pulse intro para continuar...");
		MenuBiblioMatBusqueda.menuBiblioMatBusqueda();
	}
}