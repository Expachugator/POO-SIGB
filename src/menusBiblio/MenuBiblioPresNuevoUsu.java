package menusBiblio;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Scanner;

import main.FactoriaGestores;
import materiales.GestorLibro;
import materiales.GestorPeriodico;
import materiales.GestorRevista;
import materiales.Libro;
import materiales.Periodico;
import materiales.Revista;
import prestamos.GestorPrestamo;
import prestamos.Prestamo;
import usuarios.GestorUsuario;
import usuarios.Usuario;

public class MenuBiblioPresNuevoUsu {

    //devuelve los gestores
	private static GestorUsuario gestorUsuario = FactoriaGestores.crearGestorUsuario();
	private static GestorPrestamo gestorPrestamo = FactoriaGestores.crearGestorPrestamo();
	private static GestorLibro gestorLibro = FactoriaGestores.crearGestorLibro();
	private static GestorPeriodico gestorPeriodico = FactoriaGestores.crearGestorPeriodico();
	private static GestorRevista gestorRevista = FactoriaGestores.crearGestorRevista();

	public static void seleccionUsuario(Prestamo prestamo) {

		boolean encontrado = false;

    //da la fecha de hoy y del dia de devolucion
		String fechaAlta, fechaDevolucion;
		Date fecha = new Date();
		SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
		fechaAlta = formatoFecha.format(fecha);
		fecha.setDate(fecha.getDate() + 7);
		fechaDevolucion = formatoFecha.format(fecha);

		Scanner scanner = new Scanner(System.in);

		System.out.println(
				"�A que usuario quiere prestarle el " + prestamo.getTipoMat() + " " + prestamo.getTitulo() + "?");

        //pregunta a quien se le quiere prestar
		String opcion = scanner.nextLine();

        //inicializa la lista de usuarios
		Iterator<Usuario> it = gestorUsuario.getListaUsuario().iterator();

		while (it.hasNext()) {
			Usuario u = it.next();
			if (u.getDni().equals(opcion)) {	//buscamos el usuario e imprimimos sus datos
				System.out.println("El usuario seleccionado es el siguiente:");
				System.out.println("Nombre: " + u.getNombre());
				System.out.println("Apellidos: " + u.getApellidos());

				if (u.getPrestamos() > 5) {	//Se comprueba si tiene el maximo de materiales ya prestado
					System.out.println("Numero maximo de materiales prestados ya permitido");
					MenuBiblioPres.menuBiblioPres();
				} else if (u.getSancion() != null) {	//Se comprueba si esta sancionado
					System.out.println("El usuario " + u.getNombre() + " esta sancionado hasta el " + u.getSancion());
					MenuBiblioPres.menuBiblioPres();
				} else {
					System.out.println("�Quiere prestar el material " + prestamo.getTipoMat() + " "
							+ prestamo.getTitulo() + " a " + u.getNombre() + "?");
					System.out.println("(Introduzca si/no)");
					//se confirma el prestamo
					String prestar = scanner.nextLine();
					if (prestar.equals("si")) {
						Iterator<Libro> itLib = gestorLibro.getListaLibro().iterator();
						Iterator<Periodico> itPer = gestorPeriodico.getListaPeriodico().iterator();
						Iterator<Revista> itRev = gestorRevista.getListaRevista().iterator();
						while (itLib.hasNext()) {
							Libro l = itLib.next();	//Se le asigna a este usuario si es un libro
							if (l.getIdMat() == prestamo.getIdMat()) {
								l.setPrestado(true);
								encontrado = true;
								u.setPrestamos(u.getPrestamos() + 1);
								gestorUsuario.guardarListaUsuario();
								gestorLibro.guardarListaLibro();
							}
							if (encontrado == false) {
								while (itPer.hasNext()) {
									Periodico p = itPer.next(); //Se le asigna a este usuario si es un periodico
									if (p.getIdMat() == prestamo.getIdMat()) {
										p.setPrestado(true);
										encontrado = true;
										u.setPrestamos(u.getPrestamos() + 1);
										gestorUsuario.guardarListaUsuario();
										gestorPeriodico.guardarListaPeriodico();
									}
								}
							}
							if (encontrado == false) {
								while (itRev.hasNext()) {
									Revista r = itRev.next(); //Se le asigna a este usuario si es una revista
									if (r.getIdMat() == prestamo.getIdMat()) {
										r.setPrestado(true);
										u.setPrestamos(u.getPrestamos() + 1);
										gestorUsuario.guardarListaUsuario();
										gestorRevista.guardarListaRevista();
									}
								}
							}
						}
						//Se calcula hasta cuando se realiza el prestamo, se imprime en pantalla y se guarda
						prestamo.fechaPrestamo = fechaAlta;
						prestamo.fechaDevolucion = fechaDevolucion;
						Prestamo prestamoDatos = new Prestamo(prestamo.getIdMat(), true, prestamo.getTipoMat(),
								prestamo.getTitulo(), u.getIdUsuario(), fechaAlta, prestamo.getFechaDevolucion());
						System.out.println("Fecha prestamo = " + prestamo.fechaPrestamo);
						System.out.println("Fecha devolucion = " + prestamo.fechaDevolucion);
						gestorPrestamo.nuevoPrestamo(prestamoDatos);
					} else if (prestar.equals("no")) {
						MenuBiblioPres.menuBiblioPres();
					}
				}
			}
		}
		if (prestamo.fechaPrestamo == null) {
			System.out.println("Usuario no encontrado");
			MenuBiblioPres.menuBiblioPres();
		}
	}
}