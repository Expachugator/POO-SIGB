package menusBiblio;

import java.util.Iterator;
import java.util.Scanner;

import main.FactoriaGestores;
import materiales.GestorLibro;
import materiales.Libro;

public class MenuBiblioMatListadoLibro {

    //devuelve el gestor de libros
	private static GestorLibro gestorLibro = FactoriaGestores.crearGestorLibro();

	public static void menuBiblioMatListadoLibro() {
		Scanner scanner = new Scanner(System.in);

    //se itera la lista de libros
		Iterator<Libro> it = gestorLibro.getListaLibro().iterator();

		System.out.println("Estos son nuestros libros:");

    //se imprimen
		while (it.hasNext()) {
			Libro i = it.next();
			System.out.println("Titulo: " + i.getTitulo());
			System.out.println("Autor: " + i.getAutor());
			System.out.println("IdMat: " + i.getIdMat());
			System.out.println("¿Esta prestado?" + i.getPrestado());
			System.out.println("");
		}

		System.out.println("Pulse intro para continuar...");
		String continuar = scanner.nextLine();
		MenuBiblioMatListado.menuBiblioMatListado();
	}
}