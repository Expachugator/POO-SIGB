package menusBiblio;

import java.util.Iterator;
import java.util.Scanner;

import main.FactoriaGestores;
import usuarios.GestorUsuario;
import usuarios.Usuario;

public class MenuBiblioUsuListUsu {

    //devuelve el gestor
	private static GestorUsuario gestorUsuario = FactoriaGestores.crearGestorUsuario();

	public static void menuBiblioUsuListUsu() {

		Scanner scanner = new Scanner(System.in);

    //inicializa la lista de usuarios
		Iterator<Usuario> it = gestorUsuario.getListaUsuario().iterator();

		System.out.println("Estos son nuestros usuarios:");

    //va imprimiendo los datos de cada uno
		while (it.hasNext()) {
			Usuario i = it.next();
			System.out.println("idUsu: " + i.getIdUsuario());
			System.out.println("Nombre: " + i.getNombre());
			System.out.println("Apellidos: " + i.getApellidos());
			System.out.println("DNI: " + i.getDni());
			System.out.println("");
		}

		System.out.println("Pulse intro para continuar...");
		String continuar = scanner.nextLine();
		MenuBiblioUsu.menuBiblioUsu();
	}
}