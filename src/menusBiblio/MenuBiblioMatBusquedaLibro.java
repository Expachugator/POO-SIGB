package menusBiblio;

import java.util.*;

import main.FactoriaGestores;
import materiales.GestorLibro;
import materiales.Libro;

public class MenuBiblioMatBusquedaLibro {

    //Se carga el gestor
	private static GestorLibro gestorLibro = FactoriaGestores.crearGestorLibro();

	public static void menuBiblioMatBusquedaLibro() {

		Scanner scanner = new Scanner(System.in);
		Scanner sc = new Scanner(System.in);

		//Se piden los datos por los que se quiere buscar
		
		System.out.println("Complete los campos en que quiere realizar la busqueda:");
		System.out.println("");
		System.out.println("Titulo: ");
		String busTitulo = scanner.nextLine();
		if (busTitulo.equals("")) {     //Si no se introduce nada dejamos el campo como null
			busTitulo = null;
		}
		System.out.println("Autor: ");
		String busAutor = scanner.nextLine();
		if (busAutor.equals("")) {      //Si no se introduce nada dejamos el campo como null
			busAutor = null;
		}
		System.out.println("Genero: ");
		String busGenero = scanner.nextLine();
		if (busGenero.equals("")) {     //Si no se introduce nada dejamos el campo como null
			busGenero = null;
		}
		System.out.println("Editorial: ");
		String busEditorial = scanner.nextLine();
		if (busEditorial.equals("")) {  //Si no se introduce nada dejamos el campo como null
			busEditorial = null;
		}
		System.out.println("ISBN: ");
		String busIsbn = scanner.nextLine();
		if (busIsbn.equals("")) {       //Si no se introduce nada dejamos el campo como null
			busIsbn = null;
		}

    //Se inicia la lista de libros
		Iterator<Libro> it = gestorLibro.getListaLibro().iterator();

		System.out.println("Se han encontrado los siguientes libros:");

		while (it.hasNext()) {
			Libro i = it.next();
			
			//Se hace la busqueda mirando si hemos dejado el campo en blanco o si el texto introducido
			//se encuentra entre alguno de los libros sin que tenga que ser exacto
			
			if (((busTitulo == null) || (i.getTitulo().toLowerCase().indexOf(busTitulo.toLowerCase())) != -1)
					&& ((busGenero == null) || (i.getGenero().toLowerCase().indexOf(busGenero.toLowerCase())) != -1)
					&& ((busAutor == null) || (i.getAutor().toLowerCase().indexOf(busAutor.toLowerCase())) != -1)
					&& ((busEditorial == null) || (i.getEditorial().toLowerCase().indexOf(busEditorial.toLowerCase())) != -1)
					&& ((busIsbn == null) || (i.getIsbn().indexOf(busIsbn)) != -1)) {

				System.out.println("IdMat: " + i.getIdMat());
				System.out.println("Genero: " + i.getGenero());
				System.out.println("Titulo: " + i.getTitulo());
				System.out.println("Autor: " + i.getAutor());
				System.out.println("Editorial: " + i.getEditorial());
				System.out.println("ISBN: " + i.getIsbn());
				boolean prestado = i.getPrestado();
				if (prestado == true) {
					System.out.println("Prestado: Si");
				} else {
					System.out.println("Prestado: No");
				}
				System.out.println("\n");
			}
		}
		System.out.println("Pulse intro para continuar...");
		MenuBiblioMatBusqueda.menuBiblioMatBusqueda();
	}
}
