package menusBiblio;

import java.util.*;

//Clase con el menuUsuMat que nos da acceso a otras funciones
public class MenuBiblioUsu {

	public static void menuBiblioUsu() {

		Scanner scanner = new Scanner(System.in);

		System.out.println("Seleccione una opcion");
		System.out.println("1 - Crear nuevo usuario");
		System.out.println("2 - Crear nuevo bibliotecario");
		System.out.println("3 - Listado de usuarios");
		System.out.println("4 - Listado de bibliotecarios");
		System.out.println("5 - Baja de usuario");
		System.out.println("6 - Baja de bibliotecario");
		System.out.println("0 - Atras");

    //try para evitar que se introduzcan datos de tipo distinto a int
		try {
			int opcion = scanner.nextInt();

			switch (opcion) {
			case 1:
				MenuBiblioUsuNuevoUsu.menuBiblioUsuNuevoUsuario();
				break;
			case 2:
				MenuBiblioUsuNuevoBiblio.menuBiblioUsuNuevoBiblio();
				break;
			case 3:
				MenuBiblioUsuListUsu.menuBiblioUsuListUsu();
				break;
			case 4:
				MenuBiblioUsuListBiblio.menuBiblioUsuListBiblio();
				break;
			case 5:
				MenuBiblioUsuBajaUsu.menuBiblioUsuBajaUsu();
				break;
			case 6:
				MenuBiblioUsuBajaBiblio.menuBiblioUsuBajaBiblio();
				break;
			case 0:
				MenuBiblio.menuBiblio();
				break;
			default:
				System.out.println("Introduzca una opcion correcta");
				MenuBiblioUsu.menuBiblioUsu();
				break;
			}
		} catch (Exception e) {
			System.out.println("Introduza una opcion correcta");
			MenuBiblioUsu.menuBiblioUsu();
		}
	}
}
