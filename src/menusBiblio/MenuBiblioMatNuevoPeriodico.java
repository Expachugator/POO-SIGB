package menusBiblio;

import java.util.*;

import main.FactoriaGestores;
import materiales.*;

public class MenuBiblioMatNuevoPeriodico {

    //Devuelve el gestor de periodicos
	private static GestorPeriodico gestorPeriodico = FactoriaGestores.crearGestorPeriodico();

	public static void menuBiblioMatNuevoPeriodico() {

		Scanner scanner = new Scanner(System.in);

    //Pide los datos del periodico
		System.out.println("Titulo: ");
		String titulo = scanner.nextLine();
		System.out.println("Dia: ");
		String dia = scanner.nextLine();
		System.out.println("Mes: ");
		String mes = scanner.nextLine();
		System.out.println("Año");
		String ano = scanner.nextLine();
		Boolean prestado = false;

    //los guarda en la variable periodico del tipo Periodico
		Periodico periodico = new Periodico(titulo, prestado, dia, mes, ano);

    //guarda el libro en la lista de periodicos
		gestorPeriodico.anadirPeriodico(periodico);

		System.out.println("Periodico creado con IdMat " + periodico.getIdMat() + " no olvide etiquetarlo");

		MenuBiblioMatNuevo.menuBiblioMatNuevo();
	}
}
