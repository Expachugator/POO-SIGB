package menusBiblio;

import java.util.Scanner;

//Clase con el menuUsuMat que nos da acceso a otras funciones
public class MenuBiblioSus {

	public static void menuBiblioSus() {

		Scanner scanner = new Scanner(System.in);

		System.out.println("Elija una de las siguientes opciones:");
		System.out.println("1 - Nueva suscripcion");
		System.out.println("2 - Cancelar suscripcion");
		System.out.println("3 - Ver suscripciones");
		System.out.println("0 - Atras");

    //try para evitar que se introduzcan datos de tipo distinto a int
		try {
			int opcion = scanner.nextInt();

			switch (opcion) {
			case 1:
				MenuBiblioSusNueva.menuBiblioSusNueva();
				break;
			case 2:
				MenuBiblioSusCancelar.menuBiblioSusCancelar();
				break;
			case 3:
				MenuBiblioSusVer.menuBiblioSusVer();
				break;
			case 0:
				MenuBiblio.menuBiblio();
				break;
			default:
				System.out.println("Introduzca una opcion correcta");
				MenuBiblioSus.menuBiblioSus();
				break;
			}
		} catch (Exception e) {
			System.out.println("Introduza una opcion correcta");
			MenuBiblioSus.menuBiblioSus();
		}
	}
}
