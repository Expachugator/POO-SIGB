package menusBiblio;

import java.util.Iterator;
import java.util.Scanner;

import main.FactoriaGestores;
import usuarios.Bibliotecario;
import usuarios.GestorBibliotecario;

public class MenuBiblioUsuListBiblio {

    //devuelve el gestor
	private static GestorBibliotecario gestorBibliotecario = FactoriaGestores.crearGestorBibliotecario();

	public static void menuBiblioUsuListBiblio() {

		Scanner scanner = new Scanner(System.in);

    //inicializa la lista de bibliotecarios
		Iterator<Bibliotecario> it = gestorBibliotecario.getListaBibliotecario().iterator();

		System.out.println("Estos son nuestros bibliotecarios:");

    //va imprimiendo los datos de cada uno
		while (it.hasNext()) {
			Bibliotecario i = it.next();
			System.out.println("idUsu: " + i.getIdUsuario());
			System.out.println("Nombre: " + i.getNombre());
			System.out.println("Apellidos: " + i.getApellidos());
			System.out.println("DNI: " + i.getDni());
			System.out.println("");
		}

		System.out.println("Pulse intro para continuar...");
		String continuar = scanner.nextLine();
		MenuBiblioUsu.menuBiblioUsu();
	}
}