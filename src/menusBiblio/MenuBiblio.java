package menusBiblio;

import java.util.*;
import main.Sigb;

//Clase con el menu principal del bibliotecario, desde aqui nos iremos moviendo por los diferentes menus

public class MenuBiblio {

	public static void menuBiblio() {

		Scanner scanner = new Scanner(System.in);

		System.out.println("Seleccione una opcion");
		System.out.println("1 - Gestion de materiales");
		System.out.println("2 - Gestion de prestamos");
		System.out.println("3 - Gestion de suscripciones");
		System.out.println("4 - Gestion de usuarios");
		System.out.println("5 - Red de bibliotecas asociadas");
		System.out.println("0 - Cerrar sesion");

		//try para evitar que se introduzcan datos de tipo distinto a int
		
		try {
			int opcion = scanner.nextInt();
			switch (opcion) {
			case 1:
				MenuBiblioMat.menuBiblioMat();
				break;
			case 2:
				MenuBiblioPres.menuBiblioPres();
				break;
			case 3:
				MenuBiblioSus.menuBiblioSus();
				break;
			case 4:
				MenuBiblioUsu.menuBiblioUsu();
				break;
			case 5:
				MenuBiblioRed.menuBiblioRed();
				break;
			case 0:
				Sigb.imprimirInicio();
				break;
			default:
				System.out.println("Introduzca una opcion correcta");
				MenuBiblio.menuBiblio();
				break;
			}
		} catch (Exception e) {
			System.out.println("Introduza una opcion correcta");
			MenuBiblio.menuBiblio();
		}
	}
}
