package menusBiblio;

import java.util.*;

import main.FactoriaGestores;
import materiales.GestorLibro;
import materiales.Libro;
import materiales.GestorPeriodico;
import materiales.Periodico;
import materiales.GestorRevista;
import materiales.Revista;

public class MenuBiblioMatBusquedaTodo {

    //Se cargan los gestores
	private static GestorLibro gestorLibro = FactoriaGestores.crearGestorLibro();
	private static GestorPeriodico gestorPeriodico = FactoriaGestores.crearGestorPeriodico();
	private static GestorRevista gestorRevista = FactoriaGestores.crearGestorRevista();

	public static void menuBiblioMatBusquedaTodo() {

		Scanner scanner = new Scanner(System.in);
		Scanner sc = new Scanner(System.in);

    //Se piden los datos por los que se quiere buscar
		System.out.println("Complete los campos en que quiere realizar la busqueda:");
		System.out.println("");
		System.out.println("Titulo: ");
		String busTitulo = scanner.nextLine();
		if (busTitulo.equals("")) {
			busTitulo = null;
		}

		System.out.println("Se han encontrado los siguientes materiales:");

		imprimirLibros(busTitulo);      //busca e imprime los libros
		imprimirPeriodicos(busTitulo);  //busca e imprime los periodicos
		imprimirRevistas(busTitulo);    //busca e imprime las revistas

		System.out.println("Pulse intro para continuar...");
		String salir = scanner.nextLine();

		MenuBiblioMatBusqueda.menuBiblioMatBusqueda();
	}

	public static void imprimirLibros(String busTitulo) {
		Iterator<Libro> it = gestorLibro.getListaLibro().iterator();

		while (it.hasNext()) {
			Libro i = it.next();
			if ((busTitulo == null) || (i.getTitulo().toLowerCase().indexOf(busTitulo.toLowerCase())) != -1) {

				System.out.println("IdMat: " + i.getIdMat());
				System.out.println("Genero: " + i.getGenero());
				System.out.println("Titulo: " + i.getTitulo());
				System.out.println("Autor: " + i.getAutor());
				System.out.println("Editorial: " + i.getEditorial());
				System.out.println("ISBN: " + i.getIsbn() + "\n");
			}
		}
	}

	public static void imprimirPeriodicos(String busTitulo) {

		Iterator<Periodico> it = gestorPeriodico.getListaPeriodico().iterator();

		while (it.hasNext()) {
			Periodico i = it.next();
			if ((busTitulo == null) || (i.getTitulo().toLowerCase().indexOf(busTitulo.toLowerCase())) != -1) {

				System.out.println("IdMat: " + i.getIdMat());
				System.out.println("Titulo" + i.getTitulo());
				System.out.println("Dia: " + i.getDia());
				System.out.println("Mes: " + i.getMes());
				System.out.println("Año: " + i.getAno());
			}
		}
	}

	public static void imprimirRevistas(String busTitulo) {

		Iterator<Revista> it = gestorRevista.getListaRevista().iterator();

		while (it.hasNext()) {
			Revista i = it.next();
			if ((busTitulo == null) || (i.getTitulo().toLowerCase().indexOf(busTitulo.toLowerCase())) != -1) {

				System.out.println("IdMat: " + i.getIdMat());
				System.out.println("Genero: " + i.getGenero());
				System.out.println("Titulo" + i.getTitulo());
				System.out.println("Numero: " + i.getNumero());
				System.out.println("Mes: " + i.getMes());
				System.out.println("Año: " + i.getAno());
			}
		}
	}
}