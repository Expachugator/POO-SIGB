package menusBiblio;

import java.util.Scanner;

//Clase con el menuUsuMat que nos da acceso a otras funciones
public class MenuBiblioSusVer {
	public static void menuBiblioSusVer() {

		Scanner scanner = new Scanner(System.in);

		System.out.println("Elija una de las siguientes opciones:");
		System.out.println("1 - Suscripciones activas");
		System.out.println("2 - Suscripciones canceladas");
		System.out.println("0 - Atras");

    //try para evitar que se introduzcan datos de tipo distinto a int
		try {
			int opcion = scanner.nextInt();

			switch (opcion) {
			case 1:
				MenuBiblioSusVerActivo.menuBiblioSusVerActivo();
				break;
			case 2:
				MenuBiblioSusVerCanceladas.menuBiblioSusVerCanceladas();
				break;
			case 0:
				MenuBiblioSus.menuBiblioSus();
				break;
			default:
				System.out.println("Introduzca una opcion correcta");
				MenuBiblioSusVer.menuBiblioSusVer();
				break;
			}
		} catch (Exception e) {
			System.out.println("Introduza una opcion correcta");
			MenuBiblioSusVer.menuBiblioSusVer();
		}
	}
}