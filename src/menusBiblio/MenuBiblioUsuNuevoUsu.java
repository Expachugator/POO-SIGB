package menusBiblio;

import java.util.*;
import main.FactoriaGestores;
import usuarios.GestorUsuario;
import usuarios.Usuario;

public class MenuBiblioUsuNuevoUsu {

    //devuelve el gestor
	private static GestorUsuario gestorUsuario = FactoriaGestores.crearGestorUsuario();

	public static void menuBiblioUsuNuevoUsuario() {

		Scanner scanner = new Scanner(System.in);

		int prestamos = 0;
		String sancion = null;
		
		//pide los datos
		System.out.println("Complete los siguientes campos:");
		System.out.println("Nombre: ");
		String nombre = scanner.nextLine();
		System.out.println("Apellidos: ");
		String apellidos = scanner.nextLine();
		System.out.println("DNI: ");
		String dni = scanner.nextLine();
		System.out.println("Contraseña: ");
		String contrasena = scanner.nextLine();

    //guarda los datos en un objeto usuario del tipo Usuario
		Usuario usuario = new Usuario(nombre, apellidos, contrasena, dni, prestamos, sancion);
    
    //añade el usuario
		gestorUsuario.anadirUsuario(usuario);

		MenuBiblioUsu.menuBiblioUsu();
	}
}
