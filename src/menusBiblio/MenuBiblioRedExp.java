package menusBiblio;

import java.util.Iterator;

import main.FactoriaGestores;
import materiales.GestorLibro;
import materiales.GestorPeriodico;
import materiales.GestorRevista;
import materiales.Libro;
import materiales.Periodico;
import materiales.Revista;
import red.GestorRedLibro;
import red.GestorRedPeriodico;
import red.GestorRedRevista;
import red.RedLibro;
import red.RedPeriodico;
import red.RedRevista;

public class MenuBiblioRedExp {

	private static GestorLibro gestorLibro = FactoriaGestores.crearGestorLibro();
	private static GestorPeriodico gestorPeriodico = FactoriaGestores.crearGestorPeriodico();
	private static GestorRevista gestorRevista = FactoriaGestores.crearGestorRevista();
	private static GestorRedLibro gestorRedLibro = FactoriaGestores.crearGestorRedLibro();
	private static GestorRedPeriodico gestorRedPeriodico = FactoriaGestores.crearGestorRedPeriodico();
	private static GestorRedRevista gestorRedRevista = FactoriaGestores.crearGestorRedRevista();

	public static void menuBiblioRedExp() {

		guardarLibros();
		guardarPeriodicos();
		guardarRevistas();

	}

	private static void guardarLibros() {
		Iterator<Libro> it = gestorLibro.getListaLibro().iterator();
		while (it.hasNext()) {
			Libro i = it.next();
			if (i.getPrestado() == false) {
				Libro Libro = new Libro(i.getTitulo(), i.getGenero(), i.getAutor(), i.getEditorial(), i.getIsbn(), i.getIdMat());
				gestorRedLibro.anadirExpLibro(Libro);
			}
		}
	}

	private static void guardarPeriodicos() {
		Iterator<Periodico> it = gestorPeriodico.getListaPeriodico().iterator();
		while (it.hasNext()) {
			Periodico i = it.next();
			if (i.getPrestado() == false) {
				Periodico Periodico = new Periodico(i.getTitulo(), i.getDia(), i.getMes(), i.getAno(), i.getIdMat());
				gestorRedPeriodico.anadirExpPeriodico(Periodico);
			}
		}
	}
	
	private static void guardarRevistas() {
		Iterator<Revista> it = gestorRevista.getListaRevista().iterator();
		while (it.hasNext()) {
			Revista i = it.next();
			if (i.getPrestado() == false) {
				Revista Revista = new Revista(i.getTitulo(), i.getGenero(), i.getNumero(), i.getMes(), i.getAno(), i.getIdMat());
				gestorRedRevista.anadirExpRevista(Revista);
			}
		}
	}
}
