package menusBiblio;

import java.util.Iterator;
import java.util.Scanner;

import main.FactoriaGestores;
import materiales.GestorRevista;
import materiales.Revista;

public class MenuBiblioMatListadoRevista {

    //devuelve el gestor de revistas
	private static GestorRevista gestorRevista = FactoriaGestores.crearGestorRevista();

	public static void menuBiblioMatListadoRevista() {
		Scanner scanner = new Scanner(System.in);

    //se itera la lista de revistas
		Iterator<Revista> it = gestorRevista.getListaRevista().iterator();

		System.out.println("Estos son nuestras revistas:");

        //se imprimen
		while (it.hasNext()) {
			Revista i = it.next();
			System.out.println("Titulo: " + i.getTitulo());
			System.out.println("Numero" + i.getNumero());
			System.out.println("IdMat: " + i.getIdMat());
			System.out.println("¿Esta prestado?" + i.getPrestado());
			System.out.println("");
		}

		System.out.println("Pulse intro para continuar...");
		String continuar = scanner.nextLine();
		MenuBiblioMatListado.menuBiblioMatListado();
	}
}