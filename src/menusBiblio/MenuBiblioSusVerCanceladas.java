package menusBiblio;

import java.util.*;

import main.FactoriaGestores;
import suscripciones.GestorSuscripcion;
import suscripciones.Suscripcion;

public class MenuBiblioSusVerCanceladas {

    //devuelve el gestor
	private static GestorSuscripcion gestorSuscripcion = FactoriaGestores.crearGestorSuscripcion();

	public static void menuBiblioSusVerCanceladas() {

		Scanner scanner = new Scanner(System.in);

		System.out.println("Estas son las suscripciones que han sido canceladas:");

		buscarSuscripciones();

		System.out.println("Pulse intro para continuar...");
		String salir = scanner.nextLine();

		MenuBiblioSusVer.menuBiblioSusVer();
		;
	}

	private static void buscarSuscripciones() {

    //inicia la lista
		Iterator<Suscripcion> it = gestorSuscripcion.getListaSuscripcion().iterator();

    //busca las que tienen fecha de fin y las imprime
		while (it.hasNext()) {
			Suscripcion i = it.next();
			if (i.getFechaFin() != null) {
				System.out.println("IdSus: " + i.getIdSus());
				System.out.println("Tipo de material: " + i.getTipoMat());
				System.out.println("Titulo: " + i.getTitulo());
				System.out.println("Tipo de suscripcion: " + i.getTipo());
				System.out.println("Fecha en que se suscribio: " + i.getFechaInicio());
				System.out.println("Fecha en que finalizo: " + i.getFechaFin());
			}
		}
	}
}
