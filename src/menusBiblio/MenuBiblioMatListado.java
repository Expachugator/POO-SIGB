package menusBiblio;

import java.util.*;

//Clase con el menuUsuMat que nos da acceso a otras funciones
public class MenuBiblioMatListado {

	public static void menuBiblioMatListado() {

		Scanner scanner = new Scanner(System.in);

		System.out.println("Elija de que material quiere el listado:");
		System.out.println("1 - Libros");
		System.out.println("2 - Periodicos");
		System.out.println("3 - Revistas");
		System.out.println("4 - Todo");
		System.out.println("0 - Atras");

    //try para evitar que se introduzcan datos de tipo distinto a int
		try {
			int opcion = scanner.nextInt();

			switch (opcion) {
			case 1:
				MenuBiblioMatListadoLibro.menuBiblioMatListadoLibro();
				break;
			case 2:
				MenuBiblioMatListadoPeriodico.menuBiblioMatListadoPeriodico();
				break;
			case 3:
				MenuBiblioMatListadoRevista.menuBiblioMatListadoRevista();
				break;
			case 4:
				MenuBiblioMatListadoTodo.menuBiblioMatListadoTodo();
				break;
			case 0:
				MenuBiblioMat.menuBiblioMat();
				break;
			default:
				System.out.println("Introduzca una opcion correcta");
				MenuBiblioMatListado.menuBiblioMatListado();
				break;
			}
		} catch (Exception e) {
			System.out.println("Introduza una opcion correcta");
			MenuBiblioMatListado.menuBiblioMatListado();
		}
	}
}
