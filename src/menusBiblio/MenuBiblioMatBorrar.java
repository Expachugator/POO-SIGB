package menusBiblio;

import java.util.*;

import main.FactoriaGestores;
import materiales.*;

public class MenuBiblioMatBorrar {

    //Se cargan los gestores
	private static GestorLibro gestorLibro = FactoriaGestores.crearGestorLibro();
	private static GestorPeriodico gestorPeriodico = FactoriaGestores.crearGestorPeriodico();
	private static GestorRevista gestorRevista = FactoriaGestores.crearGestorRevista();

	public static void menuBiblioMatBorrar() {

		Scanner scanner = new Scanner(System.in);

		System.out.println("Introduzca el IdMat del material a borrar: ");

    //try para evitar que se introduzcan datos de tipo distinto a int
		try {
			int opcion = scanner.nextInt();
			buscarLibro(opcion);
		} catch (Exception e) {
			System.out.println("Introduzca una opcion valida");
			MenuBiblioMatBorrar.menuBiblioMatBorrar();
		}

		MenuBiblioMat.menuBiblioMat();

	}

	//En esta clase se busca entre los libros y se muestra sus datos
	private static void buscarLibro(int opcion) {
		Iterator<Libro> it = gestorLibro.getListaLibro().iterator();

		while (it.hasNext()) {
			Libro i = it.next();
			if (i.getIdMat() == opcion) {
				System.out.println("El libro seleccionado es el siguiente:");
				System.out.println("IdMat: " + i.getIdMat());
				System.out.println("Titulo: " + i.getTitulo());
				System.out.println("Autor: " + i.getAutor());
				System.out.println("Editorial: " + i.getEditorial());
				System.out.println("ISBN: " + i.getIsbn());

				System.out.println("¿Esta seguro de borrar el libro?");
				System.out.println("(Introduzca si/no)");
				Scanner scanner = new Scanner(System.in);
				String borrar = scanner.nextLine();
				if (borrar.equals("si")) { //Si se confirma que se quiere borrar se procede y se guarda
					gestorLibro.borrarLibro(i);
					System.out.println("Material borrado");
					MenuBiblioMat.menuBiblioMat();
				} else if (borrar.equals("no")) {
					MenuBiblioMat.menuBiblioMat();
				}
			} else {
				buscarPeriodico(opcion); //Si no se encuentra buscamos entre los periodicos
			}
		}
	}

    //En esta clase se busca entre los periodicos y se muestra sus datos
	private static void buscarPeriodico(int opcion) {
		Iterator<Periodico> it = gestorPeriodico.getListaPeriodico().iterator();

		while (it.hasNext()) {
			Periodico i = it.next();
			if (i.getIdMat() == opcion) {
				System.out.println("El periodico seleccionado es el siguiente:");
				System.out.println("IdMat: " + i.getIdMat());
				System.out.println("Titulo: " + i.getTitulo());
				System.out.println("Dia: " + i.getDia());
				System.out.println("Mes: " + i.getMes());
				System.out.println("Año: " + i.getAno());

				System.out.println("¿Esta seguro de borrar el periodico?");
				System.out.println("(Introduzca si/no)");
				Scanner scanner = new Scanner(System.in);
				String borrar = scanner.nextLine();
				if (borrar.equals("si")) {
					gestorPeriodico.borrarPeriodico(i);
					MenuBiblioMat.menuBiblioMat();
				} else if (borrar.equals("no")) {
					MenuBiblioMat.menuBiblioMat();
				}
			} else {
				buscarRevista(opcion); //Si no se encuentra buscamos entre las revistas
			}
		}
	}

    //En esta clase se busca entre las revistas y se muestra sus datos
	private static void buscarRevista(int opcion) {
		Iterator<Revista> it = gestorRevista.getListaRevista().iterator();

		while (it.hasNext()) {
			Revista i = it.next();
			if (i.getIdMat() == opcion) {
				System.out.println("La revista seleccionada es la siguiente:");
				System.out.println("IdMat: " + i.getIdMat());
				System.out.println("Titulo: " + i.getTitulo());
				System.out.println("Numero: " + i.getNumero());
				System.out.println("Mes: " + i.getMes());
				System.out.println("Año: " + i.getAno());

				System.out.println("¿Esta seguro de borrar la revista?");
				System.out.println("(Introduzca si/no)");
				Scanner scanner = new Scanner(System.in);
				String borrar = scanner.nextLine();
				if (borrar.equals("si")) {
					gestorRevista.borrarRevista(i);
					MenuBiblioMat.menuBiblioMat();
				} else if (borrar.equals("no")) {
					MenuBiblioMat.menuBiblioMat();
				}
			}
		}
	}
}
