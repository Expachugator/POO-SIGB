package menusBiblio;

import java.util.*;

import main.FactoriaGestores;
import materiales.GestorRevista;
import materiales.Revista;

public class MenuBiblioMatBusquedaRevista {

    //Se carga el gestor
	private static GestorRevista gestorRevista = FactoriaGestores.crearGestorRevista();

	public static void menuBiblioMatBusquedaRevista() {

		Scanner scanner = new Scanner(System.in);
		Scanner sc = new Scanner(System.in);

    //Se piden los datos por los que se quiere buscar
		System.out.println("Complete los campos en que quiere realizar la busqueda:");
		System.out.println("");
		System.out.println("Titulo: ");
		String busTitulo = scanner.nextLine();
		if (busTitulo.equals("")) {     //Si no se introduce nada dejamos el campo como null
			busTitulo = null;
		}
		System.out.println("Genero: ");
		String busGenero = scanner.nextLine();
		if (busGenero.equals("")) {     //Si no se introduce nada dejamos el campo como null
			busGenero = null;
		}
		System.out.println("Numero: ");
		String busNumero = scanner.nextLine();
		if (busNumero.equals("")) {     //Si no se introduce nada dejamos el campo como null
			busNumero = null;
		}
		System.out.println("Mes: ");
		String busMes = scanner.nextLine();
		if (busMes.equals("")) {        //Si no se introduce nada dejamos el campo como null
			busMes = null;
		}
		System.out.println("Año: ");
		String busAno = scanner.nextLine();
		if (busAno.equals("")) {        //Si no se introduce nada dejamos el campo como null
			busAno = null;
		}

    //Se inicia la lista de revistas
		Iterator<Revista> it = gestorRevista.getListaRevista().iterator();

		System.out.println("Se han encontrado las siguientes revistas:");

		while (it.hasNext()) {
			Revista i = it.next();
			
			//Se hace la busqueda mirando si hemos dejado el campo en blanco o si el texto introducido
			//se encuentra entre alguna de las revistas sin que tenga que ser exacto
			
			if (((busTitulo == null) || (i.getTitulo().toLowerCase().indexOf(busTitulo.toLowerCase())) != -1)
					&& ((busGenero == null) || (i.getGenero().toLowerCase().indexOf(busGenero.toLowerCase())) != -1)
					&& ((busNumero == null) || (i.getNumero().equals(busNumero)))
					&& ((busMes == null) || (i.getMes().equals(busMes)))
					&& ((busAno == null) || (i.getAno().equals(busAno)))) {

				System.out.println("IdMat: " + i.getIdMat());
				System.out.println("Genero: " + i.getGenero());
				System.out.println("Titulo: " + i.getTitulo());
				System.out.println("Numero: " + i.getNumero());
				System.out.println("Mes: " + i.getMes());
				System.out.println("Año: " + i.getAno());
			}
		}

		System.out.println("Pulse intro para continuar...");
		MenuBiblioMatBusqueda.menuBiblioMatBusqueda();
	}
}