package menusBiblio;

import java.util.Iterator;
import java.util.Scanner;

import main.FactoriaGestores;
import materiales.GestorLibro;
import materiales.GestorPeriodico;
import materiales.GestorRevista;
import materiales.Libro;
import materiales.Periodico;
import materiales.Revista;

public class MenuBiblioMatListadoTodo {

    //devuelve los gestores
	private static GestorLibro gestorLibro = FactoriaGestores.crearGestorLibro();
	private static GestorPeriodico gestorPeriodico = FactoriaGestores.crearGestorPeriodico();
	private static GestorRevista gestorRevista = FactoriaGestores.crearGestorRevista();

	public static void menuBiblioMatListadoTodo() {

		Scanner scanner = new Scanner(System.in);

		imprimirLibros();       //itera la lista de libros y la imprime
		imprimirPeriodicos();   //itera la lista de periodicos y la imprime
		imprimirRevistas();     //itera la lista de revistas y la imprime

		System.out.println("Pulse intro para continuar...");
		String continuar = scanner.nextLine();
		MenuBiblioMatListado.menuBiblioMatListado();

	}

	private static void imprimirLibros() {
		Iterator<Libro> it = gestorLibro.getListaLibro().iterator();

		System.out.println("Estos son nuestros libros:");

		while (it.hasNext()) {
			Libro i = it.next();
			System.out.println("Titulo: " + i.getTitulo());
			System.out.println("Autor: " + i.getAutor());
			System.out.println("IdMat: " + i.getIdMat());
			System.out.println("¿Esta prestado?" + i.getPrestado());
			System.out.println("");
		}
	}

	private static void imprimirPeriodicos() {
		Iterator<Periodico> it = gestorPeriodico.getListaPeriodico().iterator();

		System.out.println("\nEstos son nuestros periodicos:");

		while (it.hasNext()) {
			Periodico i = it.next();
			System.out.println("Titulo: " + i.getTitulo());
			System.out.println("Fecha" + i.getDia() + "/" + i.getMes() + "/" + i.getAno());
			System.out.println("IdMat: " + i.getIdMat());
			System.out.println("¿Esta prestado?" + i.getPrestado());
			System.out.println("");
		}
	}

	private static void imprimirRevistas() {
		Iterator<Revista> it = gestorRevista.getListaRevista().iterator();

		System.out.println("\nEstas son nuestras revistas:");

		while (it.hasNext()) {
			Revista i = it.next();
			System.out.println("Titulo: " + i.getTitulo());
			System.out.println("Numero" + i.getNumero());
			System.out.println("IdMat: " + i.getIdMat());
			System.out.println("¿Esta prestado?" + i.getPrestado());
			System.out.println("");
		}
	}
}
