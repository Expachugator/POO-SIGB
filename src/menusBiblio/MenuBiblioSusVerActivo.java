package menusBiblio;

import java.util.*;

import main.FactoriaGestores;
import suscripciones.GestorSuscripcion;
import suscripciones.Suscripcion;

public class MenuBiblioSusVerActivo {
    //devuelve el gestor
	private static GestorSuscripcion gestorSuscripcion = FactoriaGestores.crearGestorSuscripcion();

	public static void menuBiblioSusVerActivo() {

		Scanner scanner = new Scanner(System.in);

		System.out.println("Estson las suscripciones activas:");

		buscarSuscripciones();

		System.out.println("Pulse intro para continuar...");
		String salir = scanner.nextLine();

		MenuBiblioSusVer.menuBiblioSusVer();
	}

	private static void buscarSuscripciones() {

    //inicializa la lista
		Iterator<Suscripcion> it = gestorSuscripcion.getListaSuscripcion().iterator();

    //comprueba e imprime todos los que no tienen fecha de fin
		while (it.hasNext()) {
			Suscripcion i = it.next();
			if (i.getFechaFin() == null) {
				System.out.println("IdSus: " + i.getIdSus());
				System.out.println("Tipo de material: " + i.getTipoMat());
				System.out.println("Titulo: " + i.getTitulo());
				System.out.println("Tipo de suscripcion: " + i.getTipo());
				System.out.println("Fecha en que se suscribio: " + i.getFechaInicio());
			}
		}
	}
}