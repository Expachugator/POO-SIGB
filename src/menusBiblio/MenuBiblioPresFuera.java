package menusBiblio;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

import main.FactoriaGestores;
import prestamos.GestorPrestamo;
import prestamos.Prestamo;

public class MenuBiblioPresFuera {

    //devuelve el gestor
	private static GestorPrestamo gestorPrestamo = FactoriaGestores.crearGestorPrestamo();

	public static void menuBiblioPresFuera() {

    //inicializa la lista de prestamos
		Iterator<Prestamo> it = gestorPrestamo.getListaPrestamo().iterator();

    //calcula y guarda el dia de hoy
		String fechaHoy;
		Date fecha = new Date();
		SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
		fechaHoy = formatoFecha.format(fecha);

		System.out.println("Estos son materiales fuera de plazo de devolucion:");
		//Se comprueba entre los materiales prestados la fecha y se imprime y esta fuera de plazo
		while (it.hasNext()) {
			Prestamo i = it.next();
			if (i.getPrestado() == true) {
				String anoDev = i.getFechaDevolucion().substring(6, 10);
				int anoDevInt = Integer.parseInt(anoDev);
				String anoHoy = fechaHoy.substring(6, 10);
				int anoHoyInt = Integer.parseInt(anoHoy);
				if (anoDevInt < anoHoyInt) {
					System.out.println("IdMat: " + i.getIdMat());
					System.out.println("Tipo de material: " + i.getTipoMat());
					System.out.println("Titulo: " + i.getTitulo());
					System.out.println("Prestado al usuario con iD: " + i.getIdUsu());
					System.out.println("Dia de devolucion: " + i.getFechaDevolucion());
					System.out.println("");
				} else if (anoDevInt == anoHoyInt) {
					String mesDev = i.getFechaDevolucion().substring(3, 5);
					int mesDevInt = Integer.parseInt(mesDev);
					String mesHoy = fechaHoy.substring(3, 5);
					int mesHoyInt = Integer.parseInt(mesHoy);
					if (mesDevInt < mesHoyInt) {
						System.out.println("IdMat: " + i.getIdMat());
						System.out.println("Tipo de material: " + i.getTipoMat());
						System.out.println("Titulo: " + i.getTitulo());
						System.out.println("Prestado al usuario con iD: " + i.getIdUsu());
						System.out.println("Dia de devolucion: " + i.getFechaDevolucion());
						System.out.println("");
					} else if (mesDevInt == mesHoyInt) {
						String diaDev = i.getFechaDevolucion().substring(0, 2);
						int diaDevInt = Integer.parseInt(diaDev);
						String diaHoy = fechaHoy.substring(0, 2);
						int diaHoyInt = Integer.parseInt(diaHoy);
						if (diaDevInt < diaHoyInt) {
							System.out.println("IdMat: " + i.getIdMat());
							System.out.println("Tipo de material: " + i.getTipoMat());
							System.out.println("Titulo: " + i.getTitulo());
							System.out.println("Prestado al usuario con iD: " + i.getIdUsu());
							System.out.println("Dia de devolucion: " + i.getFechaDevolucion());
							System.out.println("");
						}
					}
				}
			}
		}
		MenuBiblioPres.menuBiblioPres();
	}
}