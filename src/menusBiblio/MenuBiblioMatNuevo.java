package menusBiblio;

import java.util.*;

//Clase con el menuUsuMat que nos da acceso a otras funciones
public class MenuBiblioMatNuevo {

	public static void menuBiblioMatNuevo() {

		Scanner scanner = new Scanner(System.in);

		System.out.println("Seleccione el tipo de material que quiere añadir:\n");
		System.out.println("1 - Libro");
		System.out.println("2 - Periodico");
		System.out.println("3 - Revista");
		System.out.println("0 - Atras");

    //try para evitar que se introduzcan datos de tipo distinto a int
		try {
			int opcion = scanner.nextInt();

			switch (opcion) {
			case 1:
				MenuBiblioMatNuevoLibro.menuBiblioMatNuevoLibro();
				break;
			case 2:
				MenuBiblioMatNuevoPeriodico.menuBiblioMatNuevoPeriodico();
				break;
			case 3:
				MenuBiblioMatNuevoRevista.menuBiblioMatNuevoRevista();
				break;
			case 0:
				MenuBiblioMat.menuBiblioMat();
			default:
				System.out.println("Introduzca una opcion correcta");
				MenuBiblioMatNuevo.menuBiblioMatNuevo();
				break;
			}
		} catch (Exception e) {
			System.out.println("Introduza una opcion correcta");
			MenuBiblioMatNuevo.menuBiblioMatNuevo();
		}
	}
}
