package menusBiblio;

import java.util.Iterator;
import java.util.Scanner;

import main.FactoriaGestores;
import prestamos.GestorPrestamo;
import prestamos.Prestamo;

public class MenuBiblioPresListTodo {

    //devuelve el gestor
	private static GestorPrestamo gestorPrestamo = FactoriaGestores.crearGestorPrestamo();

	public static void menuBiblioPresListTodo() {

		Scanner scanner = new Scanner(System.in);

		imprimirLibro();        //busca que el tipo sea igual a libro y los imprime
		imprimirPeriodico();    //busca que el tipo sea igual a periodico y los imprime
		imprimirRevista();      //busca que el tipo sea igual a revista y los imprime

		System.out.println("Pulse intro para continuar...");
		String continuar = scanner.nextLine();
		MenuBiblioPresList.menuBiblioPresList();
	}

	private static void imprimirLibro() {
		Iterator<Prestamo> it = gestorPrestamo.getListaPrestamo().iterator();

		System.out.println("Estos son nuestros libros prestados:");

		while (it.hasNext()) {
			Prestamo i = it.next();
			if (i.getTipoMat().equals("Libro") && i.getPrestado() == true) {
				System.out.println("Titulo: " + i.getTitulo());
				System.out.println("IdMat: " + i.getIdMat());
				System.out.println("Fecha de devolucion: " + i.getFechaDevolucion());
				System.out.println("");
			}
		}
	}

	private static void imprimirPeriodico() {
		Iterator<Prestamo> it = gestorPrestamo.getListaPrestamo().iterator();

		System.out.println("Estos son nuestros periodicos prestados:");

		while (it.hasNext()) {
			Prestamo i = it.next();
			if (i.getTipoMat().equals("Periodico") && i.getPrestado() == true) {
				System.out.println("Titulo: " + i.getTitulo());
				System.out.println("IdMat: " + i.getIdMat());
				System.out.println("Fecha de devolucion: " + i.getFechaDevolucion());
				System.out.println("");
			}
		}
	}

	private static void imprimirRevista() {
		Iterator<Prestamo> it = gestorPrestamo.getListaPrestamo().iterator();

		System.out.println("Estas son nuestras revistas prestadas:");

		while (it.hasNext()) {
			Prestamo i = it.next();
			if (i.getTipoMat().equals("Revista") && i.getPrestado() == true) {
				System.out.println("Titulo: " + i.getTitulo());
				System.out.println("IdMat: " + i.getIdMat());
				System.out.println("Fecha de devolucion: " + i.getFechaDevolucion());
				System.out.println("");
			}
		}
	}
}
