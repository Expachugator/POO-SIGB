package menusBiblio;

import java.text.SimpleDateFormat;
import java.util.*;

import main.FactoriaGestores;
import suscripciones.GestorSuscripcion;
import suscripciones.Suscripcion;

public class MenuBiblioSusCancelar {

    //devuelve el gestor
	private static GestorSuscripcion gestorSuscripcion = FactoriaGestores.crearGestorSuscripcion();

	public static void menuBiblioSusCancelar() {

		Scanner scanner = new Scanner(System.in);

    //calcula el dia de hoy
		String fechaCancelacion;
		Date fecha = new Date();
		SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
		fechaCancelacion = formatoFecha.format(fecha);

		System.out.println("Introduzca el numero IdSus de la suscripcion que quiere cancelar:");
    //pide la suscripcion que se quiere cancelar
		int opcion = scanner.nextInt();

		busquedaSuscripcion(opcion, fechaCancelacion);

	}

	public static void busquedaSuscripcion(int opcion, String fechaCancelacion) {

		Scanner scanner = new Scanner(System.in);
		
		//inicializa la lista
		Iterator<Suscripcion> it = gestorSuscripcion.getListaSuscripcion().iterator();

		boolean encontrado = false;

		// Se busca la suscripcion

		while (it.hasNext()) {
			Suscripcion i = it.next();
			if (i.getIdSus() == opcion) {
				encontrado = true;
				if (i.getFechaFin() != null) {  //si ya tiene fecha de cancelacion acaba
					salir(i);
				} else {
					System.out.println("Confirmar la cancelacion a " + i.getTipoMat() + " " + i.getTitulo() + "?");
					System.out.println("(Introduzca si/no)");
					String cancelar = scanner.nextLine();
					if (cancelar.equals("si")) {
						i.setFechaFin(fechaCancelacion); // Se pone la fecha en que ha sido cancelada y se guarda
						gestorSuscripcion.guardarSuscripcion();
						System.out.println("Suscripcion del material " + i.getTitulo() + " cancelada");
						System.out.println("Pulse intro para continuar...");
						Scanner sc = new Scanner(System.in);
						String salir = sc.nextLine();
						MenuBiblioSus.menuBiblioSus();
					} else {
						MenuBiblioSus.menuBiblioSus();
					}
				}
			}
		}
		if (encontrado == false) {
			System.out.println("Suscripcion no encontrada");
			MenuBiblioSus.menuBiblioSus();
		}
	}

	private static void salir(Suscripcion i) {
		System.out.println(
				"La suscripcion del material " + i.getTitulo() + " ya estaba cancelada desde el " + i.getFechaFin());
		System.out.println("Pulse intro para continuar...");
		Scanner sc = new Scanner(System.in);
		String salir = sc.nextLine();
		MenuBiblioSus.menuBiblioSus();
	}
}