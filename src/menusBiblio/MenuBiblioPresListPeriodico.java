package menusBiblio;

import java.util.Iterator;
import java.util.Scanner;

import main.FactoriaGestores;
import prestamos.GestorPrestamo;
import prestamos.Prestamo;

public class MenuBiblioPresListPeriodico {

    //devuelve el gestor
	private static GestorPrestamo gestorPrestamo = FactoriaGestores.crearGestorPrestamo();

	public static void menuBiblioPresListPeriodico(){
		Scanner scanner = new Scanner(System.in);

    //inicializa la lista de prestamos
		Iterator<Prestamo> it = gestorPrestamo.getListaPrestamo().iterator();
		
		System.out.println("Estos son nuestros periodicos prestados:");
	
	//busca si el tipo es igual a periodico y si es correcto lo imprime	
		while(it.hasNext()){
			Prestamo i = it.next();
			if (i.getTipoMat().equals("Periodico") && i.getPrestado()== true){
				System.out.println("Titulo: " + i.getTitulo());
				System.out.println("IdMat: " + i.getIdMat());
				System.out.println("Fecha de devolucion: " + i.getFechaDevolucion());
				System.out.println("");
			}
		}
		
		System.out.println("Pulse intro para continuar...");
		String continuar = scanner.nextLine();
		MenuBiblioPresList.menuBiblioPresList();
	}
}