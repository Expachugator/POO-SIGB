package menusBiblio;

import java.util.Scanner;

import main.Sigb;

public class MenuBiblioRed {

	public static void menuBiblioRed(){
		
		Scanner scanner = new Scanner(System.in);

		System.out.println("Seleccione una opcion");
		System.out.println("1 - Cargar archivos de materiales de otras bibliotecas");
		System.out.println("2 - Exportar archivo de materiales para otra biblioteca");
		System.out.println("3 - Buscar materiales en la red de bibliotecas");
		System.out.println("0 - Atras");

		try {
			int opcion = scanner.nextInt();
			switch (opcion) {
			case 1:
				MenuBiblioRedImp.menuBiblioRedImp();
				break;
			case 2:
				MenuBiblioRedExp.menuBiblioRedExp();
				break;
			case 3:
				MenuBiblioBus.menuBiblioBus();
				break;
			case 0:
				MenuBiblio.menuBiblio();
				break;
			default:
				System.out.println("Introduzca una opcion correcta");
				MenuBiblioRed.menuBiblioRed();
				break;
			}
		} catch (Exception e) {
			System.out.println("Introduza una opcion correcta");
			MenuBiblioRed.menuBiblioRed();
		}
	}
}