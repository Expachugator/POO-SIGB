package menusBiblio;

import java.util.*;

import main.FactoriaGestores;
import materiales.*;

public class MenuBiblioMatNuevoLibro {

    //Devuelve el gestor de libros
	private static GestorLibro gestorLibro = FactoriaGestores.crearGestorLibro();

	public static void menuBiblioMatNuevoLibro() {

		Scanner scanner = new Scanner(System.in);

    //Pide los datos del libro
		System.out.println("Complete los siguientes campos:");
		System.out.println("Titulo: ");
		String titulo = scanner.nextLine();
		System.out.println("Autor: ");
		String autor = scanner.nextLine();
		System.out.println("Genero: ");
		String genero = scanner.nextLine();
		System.out.println("Editorial: ");
		String editorial = scanner.nextLine();
		System.out.println("ISBN: ");
		String isbn = scanner.nextLine();
		Boolean prestado = false;

    //los guarda en la variable libro del tipo Libro
		Libro libro = new Libro(titulo, genero, autor, prestado, editorial, isbn);

    //guarda el libro en la lista de libros
		gestorLibro.anadirLibro(libro);

		System.out.println("Libro creado con IdMat " + libro.getIdMat() + " no olvide etiquetarlo");

		MenuBiblioMatNuevo.menuBiblioMatNuevo();

	}
}