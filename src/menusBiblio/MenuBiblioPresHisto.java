package menusBiblio;

import java.util.*;

import main.FactoriaGestores;
import prestamos.GestorPrestamo;
import prestamos.Prestamo;

public class MenuBiblioPresHisto {

    //devuelve el gestor
	private static GestorPrestamo gestorPrestamo = FactoriaGestores.crearGestorPrestamo();

	public static void menuBiblioPresHisto() {

		Scanner scanner = new Scanner(System.in);

		System.out.println("Introduzca el numero idMat del material a consultar: ");

    //pide el material que se quiere consultar
		int opcion = scanner.nextInt();

    //inicializa la lista de prestamos
		Iterator<Prestamo> it = gestorPrestamo.getListaPrestamo().iterator();

		System.out.println("Este es el historial de prestamos del material: ");

    //busca el material en la lista e imprime el historial
		while (it.hasNext()) {
			Prestamo i = it.next();
			if (i.getIdMat() == opcion) {
				System.out.println("Titulo: " + i.getTitulo());
				System.out.println("IdMat: " + i.getIdMat());
				System.out.println("IdUsu: " + i.getIdUsu());
				System.out.println("Fecha de prestamo: " + i.getFechaPrestamo());
				System.out.println("Fecha de devolucion: " + i.getFechaDevolucion());
				System.out.println("");
			}
		}
		MenuBiblioPres.menuBiblioPres();
	}
}
