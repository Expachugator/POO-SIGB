package menusBiblio;

import java.util.Iterator;
import java.util.Scanner;

import main.FactoriaGestores;
import usuarios.GestorUsuario;
import usuarios.Usuario;

public class MenuBiblioUsuBajaUsu {

    //devuelve el gestor
	private static GestorUsuario gestorUsuario = FactoriaGestores.crearGestorUsuario();

	public static void menuBiblioUsuBajaUsu() {

		Scanner scanner = new Scanner(System.in);

    //pide el usuario a dar de baja
		System.out.println("Introduzca el IdUsu del usuario a dar de baja:");

    //try para evitar que se introduzcan datos de tipo distinto a int
		try {
			int opcion = scanner.nextInt();
			buscarUsuario(opcion);
		} catch (Exception e) {
			System.out.println("Introduzca una opcion valida");
			MenuBiblioUsuBajaUsu.menuBiblioUsuBajaUsu();
		}
	}

	private static void buscarUsuario(int opcion) {

		Scanner scanner = new Scanner(System.in);
		//inicializa la lista de usuarios
		Iterator<Usuario> it = gestorUsuario.getListaUsuario().iterator();

    //busca el usuario y si se confirma lo da de baja
		while (it.hasNext()) {
			Usuario i = it.next();
			if (i.getIdUsuario() == opcion) {
				System.out.println("El usuario seleccionado es el siguiente:");
				System.out.println("Nombre: " + i.getNombre());
				System.out.println("Apellidos: " + i.getApellidos());
				System.out.println("DNI: " + i.getDni());
				System.out.println("¿Seguro quiere dar de baja a este usuario? (si/no)");
				String borrar = scanner.nextLine();
				if (borrar.equals("si")) {
					gestorUsuario.borrarUsuario(i);
					System.out.println("Usuario dado de baja");
					MenuBiblioUsu.menuBiblioUsu();
				} else if (borrar.equals("no")) {
					MenuBiblioUsu.menuBiblioUsu();
				}
			}
		}
		System.out.println("Usuario no encontrado");
		MenuBiblioUsu.menuBiblioUsu();
	}
}
