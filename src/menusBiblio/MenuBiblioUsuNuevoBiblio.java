package menusBiblio;

import java.util.*;
import main.FactoriaGestores;
import usuarios.Bibliotecario;
import usuarios.GestorBibliotecario;

public class MenuBiblioUsuNuevoBiblio {

    //devuelve el gestor
	private static GestorBibliotecario gestorBibliotecario = FactoriaGestores.crearGestorBibliotecario();

	public static void menuBiblioUsuNuevoBiblio() {

		Scanner scanner = new Scanner(System.in);

    //pide los datos
		System.out.println("Complete los siguientes campos:");
		System.out.println("Nombre: ");
		String nombre = scanner.nextLine();
		System.out.println("Apellidos: ");
		String apellidos = scanner.nextLine();
		System.out.println("DNI: ");
		String dni = scanner.nextLine();
		System.out.println("Contraseña: ");
		String contrasena = scanner.nextLine();

    //guarda los datos en un objeto bibliotecario de tipo Bibliotecario
		Bibliotecario bibliotecario = new Bibliotecario(nombre, apellidos, contrasena, dni);

    //lo añade a la lista
		gestorBibliotecario.anadirBibliotecario(bibliotecario);

		MenuBiblioUsu.menuBiblioUsu();
	}
}
