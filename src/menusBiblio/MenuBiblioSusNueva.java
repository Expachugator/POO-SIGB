package menusBiblio;

import java.util.Scanner;

//Clase con el menuUsuMat que nos da acceso a otras funciones
public class MenuBiblioSusNueva {

	public static void menuBiblioSusNueva() {

		Scanner scanner = new Scanner(System.in);
		Scanner sc = new Scanner(System.in);

		System.out.println("Elija de que material quiere suscribirse:");
		System.out.println("1 - Periodico");
		System.out.println("2 - Revista");
		System.out.println("0 - Atras");

    //try para evitar que se introduzcan datos de tipo distinto a int
		try {
			int opcion = scanner.nextInt();

			switch (opcion) {
			case 1:
				MenuBiblioSusNuevaPeriodico.menuBiblioSusNuevaPeriodico();
				break;
			case 2:
				MenuBiblioSusNuevaRevista.menuBiblioSusNuevaRevista();
				break;
			case 0:
				MenuBiblioSus.menuBiblioSus();
				break;
			default:
				System.out.println("Introduzca una opcion correcta");
				MenuBiblioSusNueva.menuBiblioSusNueva();
				break;
			}
		} catch (Exception e) {
			System.out.println("Introduza una opcion correcta");
			MenuBiblioSusNueva.menuBiblioSusNueva();
		}
	}
}
