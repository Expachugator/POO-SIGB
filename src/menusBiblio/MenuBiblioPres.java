package menusBiblio;

import java.util.*;

//Clase con el menuUsuMat que nos da acceso a otras funciones
public class MenuBiblioPres {

	public static void menuBiblioPres() {

		Scanner scanner = new Scanner(System.in);

		System.out.println("Seleccione una de las siguientes opciones:");
		System.out.println("1 - Nuevo prestamo");
		System.out.println("2 - Devolver prestamo");
		System.out.println("3 - Listado de material prestado");
		System.out.println("4 - Listado de material fuera de plazo");
		System.out.println("5 - Historial de un material prestado");
		System.out.println("0 - Volver atras");

    //try para evitar que se introduzcan datos de tipo distinto a int
		try {
			int opcion = scanner.nextInt();

			switch (opcion) {
			case 1:
				MenuBiblioPresNuevo.menuBiblioPresNuevo();
				break;
			case 2:
				MenuBiblioPresDev.menuBiblioPresDev();
				break;
			case 3:
				MenuBiblioPresList.menuBiblioPresList();
				break;
			case 4:
				MenuBiblioPresFuera.menuBiblioPresFuera();
				break;
			case 5:
				MenuBiblioPresHisto.menuBiblioPresHisto();
				break;
			case 0:
				MenuBiblio.menuBiblio();
				break;
			default:
				System.out.println("Introduzca una opcion correcta");
				MenuBiblioPres.menuBiblioPres();
				break;
			}
		} catch (Exception e) {
			System.out.println("Introduza una opcion correcta");
			MenuBiblioPres.menuBiblioPres();
		}
	}

}