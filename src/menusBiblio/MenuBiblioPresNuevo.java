package menusBiblio;

import java.util.*;

import main.FactoriaGestores;
import materiales.*;
import prestamos.*;

public class MenuBiblioPresNuevo {

    //devuelve los gestores
	private static GestorLibro gestorLibro = FactoriaGestores.crearGestorLibro();
	private static GestorPeriodico gestorPeriodico = FactoriaGestores.crearGestorPeriodico();
	private static GestorRevista gestorRevista = FactoriaGestores.crearGestorRevista();

	public static void menuBiblioPresNuevo() {

		Scanner scanner = new Scanner(System.in);
		Scanner sc = new Scanner(System.in);

		System.out.println("Introduzca el numero IdMat del material a prestar: ");

    //pide el material a prestar
		int opcion = scanner.nextInt();

		buscarLibro(opcion); //buscamos si es un libro el id introducido

		System.out.println("Material no encontrado, introduzca uno correcto");
		System.out.println("Pulse intro para continuar...");
		String salir = sc.nextLine();
		MenuBiblioPres.menuBiblioPres();
	}

	private static void buscarLibro(int opcion) {
		Scanner sc = new Scanner(System.in);
		//inicializa la lista de libros
		Iterator<Libro> it = gestorLibro.getListaLibro().iterator();
		boolean encontrado = false;
		while (it.hasNext()) {
			Libro i = it.next();
			if (i.getIdMat() == opcion) {
				encontrado = true;	//si se encuentra se imprimen los datos
				System.out.println("El libro seleccionado es el siguiente:");
				System.out.println("IdMat: " + i.getIdMat());
				System.out.println("Genero: " + i.getGenero());
				System.out.println("Titulo: " + i.getTitulo());
				System.out.println("Autor: " + i.getAutor());
				System.out.println("Editorial: " + i.getEditorial());
				System.out.println("ISBN: " + i.getIsbn());
				if (i.getPrestado() == true) {	//Se comprueba si esta disponible
					System.out.println("El material no esta disponible");
					System.out.println("Pulse intro para continuar...");
					String continuar = sc.nextLine();
					MenuBiblioPres.menuBiblioPres();
				} else {
					System.out.println("¿Esta seguro de prestar el material?");
					System.out.println("(Introduzca si/no)");
					Scanner scanner = new Scanner(System.in);
					String prestar = scanner.nextLine();
					if (prestar.equals("si")) {
						Prestamo prestamo = new Prestamo(i.getIdMat(), i.getPrestado(), "Libro", i.getTitulo());
						MenuBiblioPresNuevoUsu.seleccionUsuario(prestamo); //una vez seleccionado el material vamos a por el usuario
						MenuBiblioPres.menuBiblioPres();
					} else if (prestar.equals("no")) {
						MenuBiblioPres.menuBiblioPres();
					}
				}
			}
		}
		if (encontrado == false){
			buscarPeriodico(opcion, encontrado); //si no es un libro buscamos entre los periodicos
		}
	}

	private static void buscarPeriodico(int opcion, boolean encontrado) {
		Scanner sc = new Scanner(System.in);
		Iterator<Periodico> it = gestorPeriodico.getListaPeriodico().iterator();
		while (it.hasNext()) {
			Periodico i = it.next();
			if (i.getIdMat() == opcion) {
				encontrado = true;
				System.out.println("El periodico seleccionado es el siguiente:");
				System.out.println("IdMat: " + i.getIdMat());
				System.out.println("Titulo: " + i.getTitulo());
				System.out.println("Dia: " + i.getDia());
				System.out.println("Mes: " + i.getMes());
				System.out.println("Año: " + i.getAno());
				if (i.getPrestado() == true) {
					System.out.println("El material no esta disponible");
					System.out.println("Pulse intro para continuar...");
					String continuar = sc.nextLine();
					MenuBiblioPres.menuBiblioPres();
				} else {
					System.out.println("¿Esta seguro de prestar el material?");
					System.out.println("(Introduzca si/no)");
					Scanner scanner = new Scanner(System.in);
					String prestar = scanner.nextLine();
					if (prestar.equals("si")) {
						Prestamo prestamo = new Prestamo(i.getIdMat(), i.getPrestado(), "Periodico", i.getTitulo());
						MenuBiblioPresNuevoUsu.seleccionUsuario(prestamo);
						MenuBiblioPres.menuBiblioPres();
					} else if (prestar.equals("no")) {
						MenuBiblioPres.menuBiblioPres();
					}
				}
			}
		}
		if (encontrado == false){
			buscarRevista(opcion); //si no buscamos entre las revistas
		}
	}

	private static void buscarRevista(int opcion) {
		Scanner sc = new Scanner(System.in);
		Iterator<Revista> it = gestorRevista.getListaRevista().iterator();
		while (it.hasNext()) {
			Revista i = it.next();
			if (i.getIdMat() == opcion) {
				System.out.println("La revista seleccionada es la siguiente:");
				System.out.println("IdMat: " + i.getIdMat());
				System.out.println("Genero: " + i.getGenero());
				System.out.println("Titulo: " + i.getTitulo());
				System.out.println("Numero: " + i.getNumero());
				System.out.println("Mes: " + i.getMes());
				System.out.println("Año: " + i.getAno());
				if (i.getPrestado() == true) {
					System.out.println("El material no esta disponible");
					System.out.println("Pulse intro para continuar...");
					String continuar = sc.nextLine();
					MenuBiblioPres.menuBiblioPres();
				} else {
					System.out.println("¿Esta seguro de prestar el material?");
					System.out.println("(Introduzca si/no)");
					Scanner scanner = new Scanner(System.in);
					String prestar = scanner.nextLine();
					if (prestar.equals("si")) {
						Prestamo prestamo = new Prestamo(i.getIdMat(), i.getPrestado(), "Revista", i.getTitulo());
						MenuBiblioPresNuevoUsu.seleccionUsuario(prestamo);
						MenuBiblioPres.menuBiblioPres();
					} else if (prestar.equals("no")) {
						MenuBiblioPres.menuBiblioPres();
					}
				}
			}
		}
	}
}
