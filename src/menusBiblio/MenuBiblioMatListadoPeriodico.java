package menusBiblio;

import java.util.Iterator;
import java.util.Scanner;

import main.FactoriaGestores;
import materiales.GestorPeriodico;
import materiales.Periodico;

public class MenuBiblioMatListadoPeriodico {

    //devuelve el gestor de periodicos
	private static GestorPeriodico gestorPeriodico = FactoriaGestores.crearGestorPeriodico();

	public static void menuBiblioMatListadoPeriodico() {
		Scanner scanner = new Scanner(System.in);

    //se itera la lista de periodicos
		Iterator<Periodico> it = gestorPeriodico.getListaPeriodico().iterator();

		System.out.println("Estos son nuestros periodicos:");

        //se imprimen
		while (it.hasNext()) {
			Periodico i = it.next();
			System.out.println("Titulo: " + i.getTitulo());
			System.out.println("Fecha" + i.getDia() + "/" + i.getMes() + "/" + i.getAno());
			System.out.println("IdMat: " + i.getIdMat());
			System.out.println("¿Esta prestado?" + i.getPrestado());
			System.out.println("");
		}

		System.out.println("Pulse intro para continuar...");
		String continuar = scanner.nextLine();
		MenuBiblioMatListado.menuBiblioMatListado();
	}
}